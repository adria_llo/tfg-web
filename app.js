/**
 * Node.js Login Boilerplate
 * More Info : http://kitchen.braitsch.io/building-a-login-system-in-node-js-and-mongodb/
 * Copyright (c) 2013-2016 Stephen Braitsch
 **/

var licode = true;

var http = require('http');
var https = require('https');
var express = require('express');
var session = require('express-session');
var bodyParser = require('body-parser');
var errorHandler = require('errorhandler');
var cookieParser = require('cookie-parser');
var MongoStore = require('connect-mongo')(session);
var mongoose = require('mongoose');
var fs = require('fs');
var email = require('./app/server/util/email-dispatcher.js');
var options = {
    key: fs.readFileSync('./app/server/cert/key.pem').toString(),
    cert: fs.readFileSync('./app/server/cert/cert.pem').toString()
};
mongoose.Promise = require('bluebird');
global.__base = __dirname + '/';

var isWin = /^win/.test(process.platform);
// Local
var port = 3001;
var portSSL = 3004;
if (!isWin) {
    // Production
    port = 80;
    portSSL = 443;
}

var app = express();

// App config
app.locals.pretty = true;
app.set('port', port);
app.set('portSSL', portSSL);
app.set('views', __dirname + '/app/server/views');
app.set('view engine', 'jade');
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(require('stylus').middleware({src: __dirname + '/app/public'}));
app.use(express.static(__dirname + '/app/public'));
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

// Database
var dbHost = process.env.DB_HOST || 'localhost';
var dbPort = process.env.DB_PORT || 27017;
var dbName = process.env.DB_NAME || 'node-login';
var dbURL = 'mongodb://' + dbHost + ':' + dbPort + '/' + dbName;
var dbOptions = {server: {socketOptions: {keepAlive: 1}}};
mongoose.connect(dbURL, dbOptions);

// Session
app.use(session({
        secret: 'faeb4453e5d14fe6f6d04637f78077c76c73d1b4',
        proxy: true,
        resave: true,
        saveUninitialized: true,
        store: new MongoStore({ mongooseConnection: mongoose.connection })
    })
);

// Routes
require('./app/server/routes')(app);
require('./app/server/routes-render')(app);
require('./app/server/routes-licode')(app);

// Nuve init
var N = require('./nuve');
var liConfig = require('./licode_config');
N.API.init(liConfig.nuve.superserviceID, liConfig.nuve.superserviceKey, liConfig.url);

if (licode) {
    // Clean rooms and init server
    var Rooms = require('./app/server/licode/rooms');
    Rooms.cleanRooms(function() {
        Rooms.getOrCreateRoom('lobby', function (roomId) {
            app.listen(app.get('port'));
            var server = https.createServer(options, app);
            server.listen(app.get('portSSL'), function () {
                console.log('Express server listening on port ' + app.get('port') + '. Secure on ' + app.get('portSSL') + '.');
            });

        });
    }, function (error){
        console.log(error);
        app.listen(app.get('port'));
        var server = https.createServer(options, app);
        server.listen(app.get('portSSL'), function () {
            console.log('Licode connection error.');
            console.log('Express server listening on port ' + app.get('port') + '. Secure on ' + app.get('portSSL') + '.');
        });
    });
} else {
    // Fast init without licode
    app.listen(app.get('port'));
    var server = https.createServer(options, app);
    server.listen(app.get('portSSL'), function () {
        console.log('Licode connection skipped.');
        console.log('Express server listening on port ' + app.get('port') + '. Secure on ' + app.get('portSSL') + '.');
    });
}

