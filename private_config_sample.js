var config = {};

/*********************************************************
 PRIVATE CONFIGURATION
 **********************************************************/
config.name = 'Creators name';
config.host = 'smtp.gmail.com';
config.email = "someone@gmail.com";
config.password = "password";
config.support = config.email;

/***** END *****/
var module = module || {};
module.exports = config;
