#!/bin/bash

# Force Sudo
if [[ $(/usr/bin/id -u) -ne 0 ]]; then
    echo "Run this script as root."
    exit
fi

base=$(pwd)
licode="tfg-licode"
webapp="tfg-web"
logs="logs"

function quitNode {
    pids=`pidof node`
    array=($pids)
    for pid in "${array[@]}"; do
    	kill -9 $pid
    done
    pids=`pidof nodejs`
    array=($pids)
    for pid in "${array[@]}"; do
        kill -9 $pid
    done
    echo "All Node process terminated."
}

function startNodes {
    cd $base
    mkdir -p $logs
    cd $base/$licode
    ./scripts/initLicode.sh
    cd $base/$webapp
    nodejs app.js > $base/$logs/out.log 2> $base/$logs/err.log &
    cat $base/$logs/out.log
    cat $base/$logs/err.log
}

if [ "$1" == "start" ]; then
	startNodes
elif [ "$1" == "stop" ]; then
	quitNode
elif [ "$1" == "restart" ]; then
    quitNode
	startNodes
else
	echo "Bad parameter"
fi

