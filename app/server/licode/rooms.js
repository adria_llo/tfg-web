/**
 * Created by Adri on 05/01/2017.
 */

var N = require(__base + 'nuve');
var lobbyRoom;
var lobbyRoomName = 'lobby';

/**
 * Get or create room by name
 * Return roomId
 */
exports.getOrCreateRoom = function (roomName, callback) {
    if (roomName === lobbyRoomName && lobbyRoom) {
        callback(lobbyRoom);
        return;
    }

    N.API.getRooms(function (roomlist) {
        var rooms = JSON.parse(roomlist);
        for (var index in rooms) {
            if (rooms[index].name === roomName ) {
                callback(rooms[index]._id);
                return;
            }
        }
        N.API.createRoom(roomName, function (room) {
            if (roomName === lobbyRoomName) {
                lobbyRoom = room._id;
            }
            callback(room._id);
        }, function (e) {
            console.log('Error: ', e);
        });
    });
};

/**
 * Delete rooms if empty
 */
exports.deleteRoomsIfEmpty = function (theRooms, callback) {
    if (theRooms.length === 0){
        callback(true);
        return;
    }
    var theRoom = theRooms.pop();
    N.API.getUsers(theRoom._id, function(userlist){
        var users = JSON.parse(userlist);
        if (Object.keys(users).length === 0){
            N.API.deleteRoom(theRoom._id, function(){
                if (theRooms.length > 0){
                    exports.deleteRoomsIfEmpty(theRooms, callback);
                }
            });
        } else {
            if (theRooms.length > 0){
                exports.deleteRoomsIfEmpty(theRooms, callback);
            }
        }
    });
};


/**
 * Clean lobby room
 */
exports.cleanRooms = function (success, error) {
    N.API.getRooms(function (roomlist) {
        var rooms = JSON.parse(roomlist);
        var roomsToCheck = [];
        for (var index in rooms){
            if (rooms[index].name !== lobbyRoomName){
                roomsToCheck.push(rooms[index]);
            }
        }
        exports.deleteRoomsIfEmpty (roomsToCheck, function () {
            success('done');
        });
    }, function (e){
        error(e);
    });

};

