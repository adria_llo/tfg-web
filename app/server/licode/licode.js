/**
 * Created by Adri on 05/01/2017.
 */
var Rooms = require('./rooms');
var N = require(__base + 'nuve');

/**
 * Create token for lobby
 */
exports.createLobbyToken = function (req, res) {
    var username = req.session._id.toString();
    var role = 'viewerWithData';
    Rooms.getOrCreateRoom('lobby', function (roomId) {
        N.API.createToken(roomId, username, role, function (token) {
            res.send(token);
        }, function (error) {
            console.log('No Erizo Controller found');
            console.log(error);
            res.status(400).send('No Erizo Controller found');
        });
    });
};

/**
 * Create token for roomId
 */
exports.createToken = function (req, res) {
    var username = req.session._id.toString();
    var role = 'presenter';
    var room = req.body['room'];
    if (!room) {
        room = username + '-test-room';
    }
    Rooms.getOrCreateRoom(room, function (roomId) {
        N.API.createToken(roomId, username, role, function (token) {
            res.send(token);
        }, function (error) {
            console.log('No Erizo Controller found');
            console.log(error);
            res.status(400).send('No Erizo Controller found');
        });
    });
};

/**
 * Delete room by id
 */
exports.deleteRoom = function (req, res) {
    var room = req.body['room'];
    N.API.deleteRoom(room, function (result) {
        res.status(200).send(result);
    }, function(err){
        if (err === "404 ErrorRoom does not exist") {
            res.status(200).send("OK");
        } else {
            res.status(400).send("Error");
        }
    });
};