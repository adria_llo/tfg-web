
var EM = {};
module.exports = EM;

var privateConfig = require('../../../private_config');

EM.name = privateConfig.name;
EM.host = privateConfig.host;
EM.from = privateConfig.email;
EM.password = privateConfig.password;

EM.server = require("emailjs/email").server.connect(
{
	host 	    : EM.host,
	user 	    : EM.from,
	password    : EM.password,
	ssl		    : true
});

EM.sendSignupEmail = function(user, base, callback) {
	EM.server.send({
		from         : "\"TFG " + EM.name + "\" <" + EM.from + ">",
		to           : user.email,
		subject      : 'Registration link',
		text         : EM.composeTextEmail(user,base),
		attachment   : EM.composeEmail(user,base)
	}, callback );
};
EM.composeEmail = function(user, base) {
    var link = base + "/account/validate/" + user.validationKey;
	var html = "<html><body>";
		html += "<p>Please, follow the next link for validating your account: </p><a href='"+link+"'>Validate your account!</a><br><br><p>"+EM.name+"</p>";
		html += "</body></html>";
	return  [{data:html, alternative:true}];
};
EM.composeTextEmail = function(user, base) {
    var link = base + "/account/validate/" + user.validationKey;
    return "Please, follow the next link for validating your account: "+link;
};


EM.sendResetPassword = function(user, base, token, callback) {
    EM.server.send({
        from         : "\"TFG " + EM.name + "\" <" + EM.from + ">",
        to           : user.email,
        subject      : 'Password Reset',
        text         : EM.composeTextEmailReset(user,base,token),
        attachment   : EM.composeEmailReset(user,base,token)
    }, callback );
};
EM.composeEmailReset = function(user, base, token) {
    var link = base + '/reset?token='+token;
    var html = "<html><body>";
    html += "<a href='"+link+"'>Click here to reset your password</a><br><br><p>"+EM.name+"</p>";
    html += "</body></html>";
    return  [{data:html, alternative:true}];
};
EM.composeTextEmailReset = function(user, base, token) {
    var link = base + '/reset?token='+token;
    return "Please, follow the next link for reset your password: "+link;
};