var Licode = require('./licode/licode');

module.exports = function (app) {

    // Create token for lobby room //
    app.post('/licode/createTokenLobby', Licode.createLobbyToken );

    // Create token for room
    app.post('/licode/createToken', Licode.createToken );

    // Create token for room
    app.post('/licode/deleteRoom', Licode.deleteRoom );

};
