/**
 * Created by Adri on 15/12/2016.
 */

// Mongoose user and friends plugins
var path = require('path');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var GroupSchema = new Schema({
    name: String,
    image: String,
    admin: {type : ObjectId, ref : 'User'},
    users: [ {type : ObjectId, ref : 'User'} ]
});


GroupSchema.post('save', function(group) {
    Group.populate(group, [{path: "users"}], function (err, populatedGroup) {
        // Update users
        for (var i=0; i<populatedGroup.users.length; i++){
            var user = populatedGroup.users[i];
            if (user.groups.indexOf(populatedGroup._id) == -1){
                user.groups.push(populatedGroup._id);
                user.save(function (err) {
                    if (err) {
                        throw new Error("Error saving user");
                    }
                });
            }
        }
    });
});

var Group = mongoose.model("Group", GroupSchema);

module.exports = Group;