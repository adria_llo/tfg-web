/**
 * Created by Adri on 05/04/2017
 */

var mongoose = require('mongoose');

// Schema defines how chat messages will be stored in MongoDB
var ChatSchema = new mongoose.Schema({
        from: {
            type: String,
            required: true
        },
        to: {
            type: String,
            required: true
        },
        message_body: {
            type: String,
            required: true
        }
    },
    {
        timestamps: true
    }
);

var Chat = mongoose.model("Chat", ChatSchema);

module.exports = Chat;
