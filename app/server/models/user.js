/**
 * Created by Adri on 15/12/2016.
 */

// Mongoose user and friends plugins
var path = require('path');
var mongoose = require('mongoose');
var MongooseUserPlugin = require('mongoose-user-plugin');
var friends = require("mongoose-friends");

var UserSchema = new mongoose.Schema();

UserSchema.plugin(MongooseUserPlugin);
UserSchema.plugin(friends());

UserSchema.add({
    // Added with plugin
    //  _id: ObjectId
    //  email: String
    //  passwordHash: String

    // Custom fields
    'name': String,
    'gender': String,
    'image': String,
    'groups': [{type : mongoose.Schema.ObjectId, ref : 'Group'}]
});

var User = mongoose.model("User", UserSchema);

module.exports = User;