var Users = require('./controllers/users');
var Groups = require('./controllers/groups');
var Account = require('./controllers/account');
var Chats = require('./controllers/chats');
var multer  = require('multer');
var upload = multer({ dest: 'app/public/uploads/' });

module.exports = function (app) {

// Auth needed //

    app.all('*', function (req, res) {
        // Force HTTPS
        if (!req.secure) {
            var newHost = req.headers.host.replace(app.get('port'), app.get('portSSL'));
            return res.redirect('https://' + newHost + req.url);
        }

        // Skip authentication of the next method/action
        var noAuth = [];
        noAuth.push({method: "GET", url: "/signup"});
        noAuth.push({method: "GET", url: "/reset"});
        noAuth.push({method: "GET", url: "/invalid"});
        noAuth.push({method: "POST", url: "/account"});
        noAuth.push({method: "POST", url: "/login"});
        noAuth.push({method: "GET", url: "/account/validate"});
        noAuth.push({method: "POST", url: "/account/lost-password"});
        noAuth.push({method: "POST", url: "/account/reset-password"});

        var found = false;
        var i = 0;
        var reqUrl = req.url + "";
        while (i<noAuth.length && !found){
            if ((noAuth[i].method == req.method && reqUrl.indexOf(noAuth[i].url) === 0) || reqUrl == "/"){
                found = true;
            }
            i++;
        }
        if (found || req.session.user != null){
            req.next();
        } else {
            res.redirect('/');
        }
    });

// Login Logout //

    app.post('/login', Account.login);

    app.post('/logout', Account.logout);

// Account //

    app.post('/account', upload.single('avatar'), Account.signup);

    app.get('/account/validate/:token', Account.validate);

    app.post('/account/update', upload.single('avatar'), Account.update);

// Users //

    app.get('/users/info/:id', Users.getUser);

    app.get('/users/all', Users.listAll);

    app.get('/users/friends', Users.getFriends);

    app.get('/users/pending', Users.getPendingFriends);

    app.get('/users/count/pending', Users.getPendingFriendsCount);

    app.get('/users/requested', Users.getRequestedFriends);

    app.get('/users/nofriends', Users.getNoFriends);

    app.post('/users/friend', Users.requestFriend );

    app.post('/users/delete', Users.deleteFriend );

// Groups //

    app.post('/group/create', Groups.createGroup);

    app.post('/group/leave', Groups.leaveGroup);

    app.post('/group/delete', Groups.deleteGroup);

    app.get('/group/mine', Groups.getMyGroups);

    app.get('/group/:id', Groups.getGroup);

// Chat //

    app.get('/chat/:id', Chats.renderHtmlChat);

    app.post('/chat/individual/message', Chats.createMessage );

    app.get('/chat/messages/:id/:skip', Chats.getMessages );

// Reset Password //

    app.post('/account/lost-password', Account.lostPassword );

    app.post('/account/reset-password', Account.resetPassword);

};
