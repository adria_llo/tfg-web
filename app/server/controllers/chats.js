/**
 * Created by Adri on 15/12/2016.
 */

var mongoose = require('mongoose');
var User = mongoose.model('User');
var Group = mongoose.model('Group');
var ChatModel = require('../models/chat');
var Chat = mongoose.model('Chat');
var Groups = require('./groups');

/**
 * Render chat room
 */
exports.renderHtmlChat = function (req, res) {
    var chatId = req.params.id;
    if (req.params.id === "config"){
        return res.status(200).send("");
    }
    // Find user
    User.findOne({_id: chatId}, function (err, user) {
        if (user) {
            if (!user.image) user.image = "default.jpg";
            return res.render('main/user-chat', {friend: user }, function (err, output) {
                return res.status(200).send({ html: output });
            });
        } else {
            // Find group
            Group.findOne({_id: chatId}, function (err, group) {
                if (group) {
                    Groups.populateGroup(group, function(populatedGroup){
                        if (!populatedGroup.image) populatedGroup.image = "default-group.jpg";
                        var owner = (populatedGroup.admin._id.toString() === req.session.user._id);
                        return res.render('main/group-chat', {group: populatedGroup, owner: owner }, function (err, output) {
                            if (err) console.error(err.message);
                            return res.status(200).send({ html: output });
                        });
                    });
                } else {
                    return res.status(400).send();
                }
            });
        }
    });
};

/**
 * Create new message
 */
exports.createMessage = function (req, res) {
    var chat = new Chat();
    chat.from = req.session.user._id;
    chat.to = req.body.to;
    chat.message_body = req.body.message;
    chat.save(function(err) {
        if (err) {
            res.status(400).send(err);
        }
        res.status(200).send('ok');
    });
};

/**
 * Get messages
 */
exports.getMessages = function (req, res) {
    var limit = 50;
    var skip = req.params.skip*1;
    Chat.find({$or:[ {'to': req.session.user._id, 'from': req.params.id}, {'to': req.params.id, 'from': req.session.user._id}]}).sort({createdAt: -1 }).skip(skip).limit(limit).exec( function(err, messages) {
        if (err) {
            res.status(400).send(err);
        }
        res.status(200).send({messages: messages, next: skip + limit, limit: limit});
    });
};
