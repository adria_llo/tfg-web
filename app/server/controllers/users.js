/**
 * Created by Adri on 15/12/2016.
 */

var mongoose = require('mongoose');
var UserModel = require('../models/user');
var User = mongoose.model('User');

/**
 * List All users
 */
exports.getUser = function (req, res) {
    var userId = req.params.id;
    User.findOne({_id: userId}, '_id email image name', function (err, user) {
        if (!err) {
            res.status(200).send(JSON.stringify(user));
        } else {
            res.status(400).send('Error getting user.');
        }
    });
};

/**
 * List All users
 */
exports.listAll = function (req, res) {
    User.find({}, function (err, users) {
        if (!err) {
            res.status(200).send(JSON.stringify(users));
        } else {
            res.status(400).send('Error getting all users.');
        }
    });
};

/**
 * Make friend request
 */
exports.requestFriend = function (req, res) {
    if (req.body['id']) {
        User.requestFriend(req.session.user._id, req.body['id'], function (data) {
            res.status(200).send("ok");
        });
    } else {
        res.status(400).send('Missing parameter "id"');
    }
};

/**
 * Delete friend request
 */
exports.deleteFriend = function (req, res) {
    if (req.body['id']) {
        User.findOne({_id: req.body['id']}, function (err, friend) {
            if (!err && friend) {
                User.findOne({_id: req.session._id}, function (err, user) {
                    if (!err && user) {
                        User.removeFriend(user, friend, function (data) {
                            res.status(200).send("ok");
                        });
                    } else {
                        res.status(400).send('Error sending request.');
                    }
                });
            } else {
                res.status(400).send('Error sending request.');
            }
        });
    } else {
        res.status(400).send('Missing parameter "id"');
    }
};

/**
 * Get all friends
 */
exports.getFriends = function (req, res) {
    User.getAcceptedFriends(req.session.user, {}, {email: 1, name: 1, gender: 1, image: 1}, function (err, friendships) {
        if (!err) {
            res.status(200).send(JSON.stringify(friendships));
        } else {
            res.status(400).send("Error");
        }
    });
};


/**
 * Get pending friends requests
 */
exports.getPendingFriends = function (req, res) {
    User.getPendingFriends(req.session.user, {}, {email: 1, name: 1}, function (err, friendships) {
        if (!err) {
            res.status(200).send(JSON.stringify(friendships));
        } else {
            res.status(400).send("Error");
        }
    });
};

/**
 * Get pending friends count
 */
exports.getPendingFriendsCount = function (req, res) {
    User.getPendingFriends(req.session.user, {}, {email: 1}, function (err, friendships) {
        if (!err) {
            var ret = {count: friendships.length};
            res.status(200).send(JSON.stringify(ret));
        } else {
            res.status(400).send("Error");
        }
    });
};

/**
 * Get requested friends requests
 */
exports.getRequestedFriends = function (req, res) {
    User.getRequestedFriends(req.session.user, {}, {email: 1}, function (err, friendships) {
        if (!err) {
            res.status(200).send(JSON.stringify(friendships));
        } else {
            res.status(400).send("Error");
        }
    });
};

/**
 * Get all users no friends (including requested ones)
 */
exports.getNoFriends = function (req, res) {
    // Get all users except me
    User.find({email: {$ne: req.session.user.email}}, '_id email image name', { sort:{ email: -1}}, function (err, users) {
        if (err) {
            res.status(400).send("Error finding users.");
        } else {
            // Filter accepted friends and mark pending ones
            User.getFriends(req.session.user, {}, {email: 1, name: 1, image: 1}, function (err, friendships) {
                if (err) {
                    res.status(400).send("Error finding users.");
                } else {
                    var noFriends = [];
                    for (var j = 0; j < users.length; j++) {
                        var _id = users[j]._id;
                        var email = users[j].email;
                        var name = users[j].name;
                        var image = users[j].image;
                        var status = "";
                        for (var i = 0; i < friendships.length; i++) {
                            if (friendships[i].friend.email === email) {
                                status = friendships[i].status;
                                break;
                            }
                        }
                        var requested = (status === "requested");
                        if (status != "accepted") {
                            noFriends.push({_id: _id, email: email, requested: requested, image: image, name: name})
                        }
                    }
                    res.status(200).send(JSON.stringify(noFriends));
                }
            });
        }
    });
};
