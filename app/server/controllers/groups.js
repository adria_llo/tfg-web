/**
 * Created by Adri on 15/12/2016.
 */

var mongoose = require('mongoose');
var GroupModel = require('../models/group');
var User = mongoose.model('User');
var Group = mongoose.model('Group');
var Account = require('./account');

/**
 * Create a new group
 */
exports.populateGroup = function (group, callback) {
    Group.populate(group, [{path: "users", select: '_id name email image'}, {path: "admin", select: '_id'}], function (err, populatedGroup) {
        if (!err) {
            callback(populatedGroup);
        } else {
            callback(null);
        }
    });
};

/**
 * Create a new group, or edit if the group exists
 */
exports.createGroup = function (req, res) {
    var groupId = req.body['groupId'];
    var name = req.body['name'];
    var ids = req.body['ids'].split(",");
    ids.push(req.session.user._id);

    var saveGroup = function (group){
        // save the group
        group.save(function (err) {
            if (!err) {
                exports.populateGroup(group, function(populatedGroup){
                    if (populatedGroup){
                        return res.status(200).send(populatedGroup);
                    } else {
                        return res.status(400).send("Error creating group!");
                    }
                });
            } else {
                return res.status(400).send(err);
            }
        });
    };

    if (groupId){
        Group.findOne({_id: groupId}, function (err, group){
            if (group){
                group.name = name;
                group.users = ids;
                saveGroup(group);
            } else {
                return res.status(400).send(err);
            }
        });
    } else {
        // create a new group
        var group = Group({
            name: name,
            admin: req.session.user._id,
            users: ids
        });
        saveGroup(group);
    }
};

/**
 * Leave from group
 */
exports.leaveGroup = function (req, res) {
    var groupId = req.body['id'];
    var userId = req.session.user._id;

    // Delete from group
    Group.findOne({_id: groupId},function (err, group) {
        if (group){
            group.users.remove(userId);
            group.save(function (err) {
                if (!err) {
                    // Delete from user
                    User.findOne({_id: userId},function (err, user) {
                        if (user){
                            user.groups.remove(groupId);
                            user.save(function (err) {
                                if (!err) {
                                    return res.status(200).send("OK");
                                } else {
                                    return res.status(400).send(err);
                                }
                            });
                        } else {
                            return res.status(400).send("Error leaving group!");
                        }
                    });
                } else {
                    return res.status(400).send(err);
                }
            });
        } else {
            return res.status(400).send("Error leaving group!");
        }
    });
};

/**
 * Delete group
 */
exports.deleteGroup = function (req, res) {
    var groupId = req.body['id'];
    var userId = req.session.user._id;
    // Delete group
    Group.findOneAndRemove({_id: groupId},function (err) {
        if (!err){
            // Delete user relation
            User.findOne({_id: userId},function (err, user) {
                if (user){
                    user.groups.remove(groupId);
                    user.save(function (err) {
                        if (!err) {
                            return res.status(200).send("OK");
                        } else {
                            return res.status(400).send(err);
                        }
                    });
                } else {
                    return res.status(400).send("Error deleting group!");
                }
            });
        } else {
            return res.status(400).send("Error deleting group!");
        }
    });
};

/**
 * Get my groups
 */
exports.getMyGroups = function(req, res){
    Account.findUser({_id: req.session._id}, function(err, user){
        if (user && user.groups.length > 0){
            User.populate(user, {path: "groups", select: '_id name image'}, function (err, popuUser) {
                if (!err) {
                    return res.status(200).send(popuUser.groups);
                } else {
                    return res.status(400).send();
                }
            });
        } else {
            return res.status(200).send([]);
        }
    });
};

/**
 * Get Group by Id
 */
exports.getGroup = function(req, res){
    var groupId = req.params.id;
    Group.findOne({_id: groupId},function (err, group) {
        if (group){
            exports.populateGroup(group, function(populatedGroup){
                if (populatedGroup){
                    return res.status(200).send(populatedGroup);
                } else {
                    return res.status(400).send("Error getting group!");
                }
            });
        } else {
            return res.status(400).send("Error getting group!");
        }
    });
};
