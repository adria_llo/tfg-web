/**
 * Created by Adri on 15/12/2016.
 */

var mongoose = require('mongoose');
var UserModel = require('../models/user');
var User = mongoose.model('User');
var EM = require('../util/email-dispatcher');
var crypto = require('crypto');

/**
 * Main find user
 */
exports.findUser = function (options, callback) {
    User.findOne(options, '_id email name gender validated image groups', callback);
};

/**
 * Login
 */
exports.login = function (req, res) {
    User.isValidUserPassword(req.body['user'], req.body['password'], function (err, user, msg) {
        if (user) {
            User.findOne({_id: user._id}, "_id email name gender validated image groups passwordHash", function (err, user) {
                req.session.user = user;
                req.session._id = user._id;
                if (req.body['remember-me'] == 'true') {
                    res.cookie('user', user.email, {maxAge: 900000});
                    res.cookie('pass', user.passwordHash, {maxAge: 900000});
                }
                return res.status(200).send(user);
            });
        } else {
            return res.status(400).send("Error login");
        }
    });
};

/**
 * Login with Hash
 */
exports.autoLogin = function (req, callback) {
    exports.findUser({email: req.cookies.user}, function (err, user) {
        if (user && user.passwordHash == req.cookies.pass) {
            callback(user);
        } else {
            callback(null);
        }
    });
};

/**
 * Logout
 */
exports.logout = function (req, res) {
    res.clearCookie('user');
    res.clearCookie('pass');
    req.session.destroy(function (e) {
        res.status(200).send('ok');
    });
};

/**
 * Sign up
 */
exports.signup = function (req, res) {
    User.signup(req.body['email'], req.body['pass'], 'en', function (err, user) {
        if (!err) {
            // Extra fields
            user.name = req.body['name'];
            user.gender = req.body['gender'];
            user.image = (req.file) ? req.file.filename : "default.jpg";
            user.save(function (err) {
                if (err) throw err;
                // Send validation link email
                var base = req.protocol + "://" + req.get('host');
                EM.sendSignupEmail(user, base, function(e){
                    if (!e){
                        res.status(200).send('ok');
                    }	else{
                        res.status(400).send('Unable to send registration email.');
                    }
                });
            });
        } else {
            return res.status(400).send(err.message);
        }
    });
};

/**
 * Validate
 */
exports.validate = function (req, res) {
    var token = req.params.token;
    return User.accountValidator(token, function (err, user) {
        if (err) {
            res.render('invalid'); // TODO
        } else {
            req.session.user = user;
            req.session._id = user._id;
            res.redirect('/home');
        }
    });
};

/**
 * Update
 */
exports.update = function (req, res) {
    var newPass = null;
    exports.findUser({email: req.session.user.email}, function (err, user) {
        if (user) {
            if (req.body['gender']){
                user.gender = req.body['gender'];
            }
            if (req.body['name']){
                user.name = req.body['name'];
            }
            if (req.body['pass']){
                newPass = req.body['pass'];
            }
            user.image = (req.file) ? req.file.filename : ((user.image) ? user.image : "default.jpg");
            user.save(function (err) {
                if (!err) {
                    // Now change password
                    if (newPass != null){
                        user.updatePassword(newPass, function(e){
                            if (!e) {
                                req.session.user = user;
                                return res.status(200).send(user);
                            } else {
                                return res.status(400).send();
                            }
                        });
                    } else {
                        req.session.user = user;
                        return res.status(200).send(user);
                    }
                } else {
                    return res.status(400).send();
                }
            });
        } else {
            return res.status(400).send();
        }
    });
};

/**
 * Lost password
 */
exports.lostPassword = function (req, res) {
    // Look up the user's account via their email
    exports.findUser({email: req.body['email']}, function (err, user) {
        if (user) {
            var token = crypto.randomBytes(32).toString('hex');
            var base = req.protocol + "://" + req.get('host');
            EM.sendResetPassword(user, base, token,function (e) {
                if (!e) {
                    // Set session email for reset password
                    req.session.reset = {};
                    req.session.reset.token = token;
                    req.session.reset.email = req.body['email'];
                    res.status(200).send('ok');
                } else {
                    res.status(400).send('Unable to dispatch password reset');
                }
            });
        } else {
            res.status(400).send('email-not-found');
        }
    });
};

/**
 * Reset password
 */
exports.resetPassword = function (req, res) {
    // Look up the user's account via their email
    var token = req.body['token'];
    var nPass = req.body['pass'];
    var email = req.session.reset.email;
    if (token !== req.session.reset.token || !email){
        return res.status(400).send('no-requested');
    }
    req.session.destroy();
    console.log(nPass);
    exports.findUser({email: email}, function (err, user) {
        if (user) {
            user.updatePassword(nPass, function(e){
                if (!e) {
                    return res.status(200).send('ok');
                } else {
                    return res.status(400).send('error');
                }
            });
        } else {
            return res.status(400).send('no-found');
        }
    });
};