var Users = require('./controllers/users');
var Account = require('./controllers/account');
var privateConfig = require('../../private_config');

module.exports = function (app) {

// main login page //

    app.get('/', function (req, res) {
        // check if the user's credentials are saved in a cookie or session //
        if (req.session.user != null) {
            res.redirect('/home');
        } else if (req.cookies.user == undefined || req.cookies.pass == undefined) {
            res.render('login');
        } else {
            // attempt automatic login //
            Account.autoLogin(req, function (user) {
                if (user){
                    req.session.user = user;
                    res.redirect('/home');
                } else {
                    res.render('login');
                }
            });
        }
    });

// logged-in user homepage //

    app.get('/home', function (req, res) {
        res.render('home', {
            user: req.session.user,
            settings: false,
            support: privateConfig.support
        });
    });

    app.get('/home/config', function (req, res) {
        res.render('home', {
            user: req.session.user,
            settings: true,
            support: privateConfig.support
        });
    });

    app.get('/home/:id', function (req, res) {
        res.render('home', {
            user: req.session.user,
            chatId: req.params.id,
            settings: false,
            support: privateConfig.support
        });
    });

// creating new accounts //

    app.get('/signup', function (req, res) {
        res.render('signup');
    });

// Reset password //

    app.get('/reset', function (req, res) {
        res.render('reset');
    });

    app.get('*', function (req, res) {
        res.render('404');
    });

};
