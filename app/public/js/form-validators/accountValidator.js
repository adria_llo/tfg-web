function AccountValidator() {
// build array maps of the form inputs & control groups //

    this.formFields = [$('#name-tf'), $('#email-tf'), $('#pass-tf'), $('#pass2-tf')];
    this.controlGroups = [$('#name-cg'), $('#email-cg'), $('#pass-cg')];

// bind the form-error modal window to this controller to display any errors //

    this.alert = $('.modal-form-errors');
    this.alert.modal({show: false, keyboard: true, backdrop: true});

    this.validateName = function (s) {
        return s.length >= 3;
    };

    this.validatePassword = function (p1) {
        if (typeof global != 'undefined') {
            // Logged users, only check if not empty
            return p1.length == 0 || p1.length >= 6;
        } else {
            return p1.length >= 6;
        }
    };

    this.validateMatchPassword = function (p1, p2){
        if (typeof global != 'undefined' && p1.length > 0) {
            // Logged users, only check if not empty
            if (p1.length > 0) {
                return p1 === p2;
            } else {
                return true;
            }
        } else {
            return p1 === p2;
        }
    };

    this.validateEmail = function (e) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(e);
    };

    this.validateGender = function () {
        return $("#maleRadio").hasClass('active') || $("#femaleRadio").hasClass('active');
    };

    this.showErrors = function (a) {
        console.log("show errors");
        $('.modal-form-errors .panel-body p').text('Please correct the following problems:');
        var ul = $('.modal-form-errors .panel-body ul');
        ul.empty();
        for (var i = 0; i < a.length; i++) ul.append('<li>' + a[i] + '</li>');
        this.alert.modal('show');
    }

}

AccountValidator.prototype.showInvalidEmail = function () {
    this.controlGroups[1].addClass('error');
    this.showErrors(['That email address is already in use.']);
};

AccountValidator.prototype.validateForm = function () {
    var e = [];
    for (var i = 0; i < this.controlGroups.length; i++) this.controlGroups[i].removeClass('error');
    if (this.validateName(this.formFields[0].val()) == false) {
        this.controlGroups[0].addClass('error');
        e.push('Please Enter Your Name');
    }
    if (this.validateEmail(this.formFields[1].val()) == false) {
        this.controlGroups[1].addClass('error');
        e.push('Please Enter A Valid Email');
    }
    if (this.validatePassword(this.formFields[2].val()) == false) {
        this.controlGroups[2].addClass('error');
        e.push('Password Should Be At Least 6 Characters');
    }
    if (this.validateMatchPassword(this.formFields[2].val(), this.formFields[3].val()) == false) {
        this.controlGroups[2].addClass('error');
        e.push('The passwords doesn\'t match');
    }
    if (this.validateGender() == false) {
        e.push('Please select the gender');
    }
    if (e.length) this.showErrors(e);
    return e.length === 0;
};

	