
$(document).ready(function(){
	
	var rv = new ResetValidator();

	$.urlParam = function(name){
		var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
		if (results==null){
			return null;
		}
		else{
			return results[1] || 0;
		}
	};

	$('#set-password-form').ajaxForm({
		beforeSubmit : function(formData, jqForm, options){;
			rv.hideAlert();
			if (rv.validatePassword($('#pass-tf').val()) == false){
				return false;
			} 	else{
				$(".modal-footer button").hide();
				return true;
			}
		},
		success	: function(responseText, status, xhr, $form){
			rv.showSuccess("Your password has been reset.");
			setTimeout(function(){ window.location.href = '/'; }, 2000);
		},
		data: {
			token: $.urlParam('token')
		},
		error : function(e){
			console.log(e);
			if (e.responseText === "no-requested"){
				rv.showAlert("Did you requested the password reset?<br>Please make sure that you open the reset link with <b>the same browser</b> that has requested the password reset.");
			} else if (e.responseText === "no-found"){
				rv.showAlert("Error. User not found");
			} else {
				rv.showAlert("I'm sorry something went wrong, please try again later or contact directly to: ");
			}
			$(".modal-footer button").show();
		}
	});

	$('#pass-tf').focus();

});