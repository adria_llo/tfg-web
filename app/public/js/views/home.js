var mainController;
var notificationController;
var licodeController;
var chatsController;

$(document).ready(function(){
	global.friends = [];
	global.online = [];
    global.incall = false;

	// Init controllers
    var sc = new SidebarController();
	mainController = new HomeController();
	notificationController = new NotificationController();
	licodeController = new LicodeController(sc);
	chatsController = new ChatsController();

	mainController.addController(licodeController);
	mainController.addController(chatsController);
	mainController.addController(new WelcomeController());
	// Top controllers
	mainController.addController(new NavController());
	mainController.addController(new ConfigController());
	// Sidebar controllers
	mainController.addController(sc);
	mainController.addController(new FriendsController());
	mainController.addController(new GroupController());
	mainController.addController(new DeleteController());

	// Init functions
	mainController.refreshAll(function(){
		if (global.chatId){
			sc.openChat(global.chatId, false);
			global.chatId = null;
		}
	});

	// Init functions
	notificationController.askNotificationsCount();

	// Sounds setup
	ion.sound({
		sounds: [
			{
				name: "iphone"
			},
			{
				name: "beep"
			}
		],
		volume: 0.2,
		loop: 20,
		path: "/sounds/",
		preload: true
	});

	// Back / Forward
	window.onpopstate = function(event) {
		window.location.href = document.location;
	};

	// Resize window
	$( window ).resize(function() {
		mainController.windowResize();
	});

	//window.onbeforeunload = function() {
	//	return "Dude, are you sure you want to leave? Think of the kittens!";
	//}
});

/*
 * Notifications permision
 */
//Notification.requestPermission().then(function(result) {
//	console.log("Browser notification permission: " + result);
//});
