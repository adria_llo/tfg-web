$(document).ready(function () {

    var av = new AccountValidator();
    var sc = new SignupController();

    $('#account-form').ajaxForm({
        beforeSubmit: function (formData, jqForm, options) {
            return av.validateForm();
        },
        success: function (responseText, status, xhr, $form) {
            localStorage.setItem("tmp-email", responseText);
            if (status == 'success') $('.modal-alert').modal('show');
        },
        error: function (e) {
            av.showInvalidEmail();
        }
    });
    $('#name-tf').focus();

    // Avatar file upload
    $("#avatar-signup").fileinput({
        overwriteInitial: true,
        maxFileSize: 10000,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
        removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: '<img src="/uploads/default.jpg" alt="Your Avatar" style="width:160px">',
        layoutTemplates: {
            main2: '<div class="col-sm-8">{preview}</div> <div class="col-sm-2" style="height: 177px;"><div class="center-vertical">{remove} {browse}</div></div>',
            actions: '',
            footer: ''
        },
        allowedFileExtensions: ["jpg", "png", "gif"]
    });

// setup the alert that displays when an account is successfully created //

    $('.modal-alert').modal({show: false, keyboard: false, backdrop: 'static'});
    $('.modal-alert .modal-header h4').text('Email verification');
    $('.modal-alert .modal-body p').html('We have sent you an email with a verification link.<br>You need to validate your account before login.');

});