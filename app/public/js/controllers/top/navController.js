function NavController() {
    // bind event listeners to button clicks //
    var that = this;

    /**
     * HOME Button click
     */
    $('#btn-home').click(function () {
        $(".user-tab").removeClass('hover');
        if ($("#btn-home").hasClass("my-focus")) {
            // Do nothing
        } else {
            // Show main pane
            $("#top-buttons-navbar").find("button").removeClass("my-focus");
            $("#btn-home").addClass("my-focus");
            $(".main-pane").hide();
            $("#home-pane").show();
            history.pushState('', 'New Page Title', "/home");
        }
    });

    /**
     * CONFIG Button click
     */
    $('#btn-config').click(function () {
        $(".user-tab").removeClass('hover');
        if ($("#btn-config").hasClass("my-focus")) {
            // Do nothing
        } else {
            // Show settings pane
            $("#top-buttons-navbar").find("button").removeClass("my-focus");
            $("#btn-config").addClass("my-focus");
            $(".main-pane").hide();
            $("#settings-pane").show();
            history.pushState('', 'New Page Title', "/home/config");
        }
    });

    /**
     * NOTIFICATIONS Button click
     */
    $("#btn-notifications").click(function () {
        if ($("#btn-notifications").hasClass("my-focus")) {
            // Hide notifications
            $("#btn-notifications").removeClass("my-focus");
            $("#notifications-li .notification-container").hide();
        } else {
            // Show notifications
            $("#btn-notifications").addClass("my-focus");
            $("#notifications-li .notification-container").fadeToggle(300);
            $("#notifications-li .notification-loader").show();
            $("#notifications-li .notification-list").hide();
            notificationController.load(function(status){
                $("#notifications-li .notification-loader").hide();
                $("#notifications-li .notification-list").show();
            });
        }
        return false;
    });

    $(document).click(function () {
        $("#notifications-li .notification-container").hide();
        $("#btn-notifications").removeClass("my-focus");
    });

    $("#notifications-li .notification-container").click(function () {
        return false
    });

    /**
     * LOGOUT Button click
     */
    $('#btn-logout').click(function () {
        $.ajax({
            url: "/logout",
            type: "POST",
            data: {logout: true},
            success: function (data) {
                window.location.href = '/';
            },
            error: function (jqXHR) {
                console.log(jqXHR.responseText + ' :: ' + jqXHR.statusText);
            }
        });
    });

    // Toggle wrapper
    $("#menu-toggle").click(function (e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
        $("#menu-toggle").toggleClass("toggled");
    });

    that.refreshUserData = function () {
        // user name
        if (global.user.name) {
            $("#navUserName").html(global.user.name);
        } else {
            $("#navUserName").html(global.user.email);
        }
        // Image
        if (global.user.image) {
            var displayImage = (global.user.image) ? global.user.image : "default.jpg";
            $("#avatar-image").attr("style", 'width: 50px; height: 50px; background: url(/uploads/' + displayImage + ') no-repeat center; background-size: cover;');
        }
    };

    that.refreshNotifications = function(count){
        $("#notifications-li .notification-count").html(count);
        if (count > 0){
            $("#notifications-li .notification-count").show();
        } else {
            $("#notifications-li .notification-count").hide();
        }
    };

    that.refreshUserData();

    return that;
}