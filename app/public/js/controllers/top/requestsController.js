function RequestsController() {
    // bind event listeners to button clicks //
    var that = this;
    return that;
}

function acceptFriend(id) {
    var action = $("#req"+id).attr('data-action');
    if (action === "waiting"){
        $("#req"+id).attr('data-action', "sending");
        $("#req"+id+" btn-success").blur();
        $("#req"+id+" btn-success").html("Sending");
        $.ajax({
            url: "/users/friend",
            type: "POST",
            data: {id: id},
            success: function (data) {
                $("#req"+id).remove();
                refreshContacts();
                checkEmptyRequests();
            },
            error: function (err) {
                $("#req"+id).attr('data-action', "waiting");
                console.log(err);
            }
        });
    }
}

function rejectFriend(id) {
    var action = $("#req"+id).attr('data-action');
    if (action === "waiting"){
        $("#req"+id).attr('data-action', "sending");
        $("#req"+id+" btn-danger").blur();
        $("#req"+id+" btn-danger").html("Sending");
        $.ajax({
            url: "/users/delete",
            type: "POST",
            data: {id: id},
            success: function (data) {
                $("#req"+id).remove();
                checkEmptyRequests();
            },
            error: function (err) {
                $("#req"+id).attr('data-action', "waiting");
                console.log(err);
            }
        });
    }
}

function checkEmptyRequests() {
    if ($("#requests-list").html() == "") {
        $("#requests-list").append('<li class="list-group-item container-fluid"><p class="left">You don\'t have any friends request</p></li>');
    }
}

/*
 *   Show
 */
$('#requestsModal').on('show.bs.modal', function (event) {
    $.ajax({
        url: "/users/pending",
        type: "GET",
        dataType: "json",
        data: {logout: true},
        success: function (users) {
            if (users) {
                $("#requests-list").html('');
                for (var i = 0; i < users.length; i++) {
                    var addBut = '<a href="#" onclick="acceptFriend(\'' + users[i]._id + '\')" class="btn right btn-success btn-xs right10">Accept</a>';
                    var rejectBut = '<a href="#" onclick="rejectFriend(\'' + users[i]._id + '\')" class="btn right btn-danger btn-xs">Reject</a>';
                    $("#requests-list").append('<li id="req'+users[i]._id+'" data-action="waiting" class="list-group-item container-fluid"><p class="left">' + users[i].friend.email + '</p>' + rejectBut + addBut + '</li>');
                }
                checkEmptyRequests();
            }
        },
        error: function () {
            alert('error');
        }
    });
});

/*
 *   Hide
 */
$('#requestsModal').on('hide.bs.modal', function (event) {

});
