function LicodeController(sc) {
    // bind event listeners to button clicks //
    var that = this;
    var callingController = new CallingController(that);
    var callController;
    var sidebarController = sc;

    var lobby;
    var lobbyStream;

    that.getCallingController = function() {
        return callingController;
    };

    that.getCallController = function() {
        return callController;
    };

    /**
     * Init lobby room
     */
    that.init = function(){
        var config = {audio: false, video: false, data: true };
        lobbyStream = Erizo.Stream(config);
        $.ajax({
            url: "/licode/createTokenLobby",
            type: "POST",
            cache: false,
            success: function (response) {
                var token = response;
                lobby = Erizo.Room({token: token});

                lobbyStream.addEventListener('access-accepted', function () {
                    var subscribeToStreams = function (streams) {
                        var cb = function (evt){
                            console.log('Bandwidth Alert', evt.msg, evt.bandwidth);
                        };
                        for (var index in streams) {
                            var stream = streams[index];
                            var attr = stream.getAttributes();
                            // Subscribe only to my friends stream
                            if (lobbyStream.getID() !== stream.getID() && global.friends.indexOf(attr.id) != -1) {
                                that.attributesUpdated(stream);
                                lobby.subscribe(stream, {slideShowMode: false});
                                stream.addEventListener('bandwidth-alert', cb);
                                stream.addEventListener('stream-data', that.receivedMessage);
                                stream.addEventListener("stream-attributes-update", that.attributesUpdated);
                            }
                        }
                    };

                    lobby.addEventListener('room-connected', function (roomEvent) {
                        lobby.publish(lobbyStream, {}, {id: global.user._id}, function(){
                            $("#avatar-image").removeClass("offline").addClass("online");
                            subscribeToStreams(roomEvent.streams);
                        });
                    });

                    lobby.addEventListener('stream-subscribed', function(streamEvent) {
                    });

                    lobby.addEventListener('stream-added', function (streamEvent) {
                        var streams = [];
                        streams.push(streamEvent.stream);
                        subscribeToStreams(streams);
                    });

                    lobby.addEventListener('stream-removed', function (streamEvent) {
                        var attr = streamEvent.stream.getAttributes();
                        mainController.userStatusUpdated('offline',attr.id);
                    });

                    lobby.addEventListener('stream-failed', function (){
                        console.log('Stream Failed, act accordingly');
                    });

                    lobby.connect();

                });

                lobbyStream.init();
            },
            error: function () {
                console.log('Error entering lobby!');
            }
        });
    };

    /**
     * User attributes update
     */
    that.attributesUpdated = function(streamEvent){
        var attr = (streamEvent.stream) ? streamEvent.stream.getAttributes() : streamEvent.getAttributes();
        if (attr.busy){
            mainController.userStatusUpdated('busy',attr.id);
        } else {
            mainController.userStatusUpdated('online',attr.id);
        }
    };


    /**
     * User status update
     */
    that.userStatusUpdated = function(type, userId){
        if (type === 'online'){
            global.online.push(userId);
        } else {
            var index = global.online.indexOf(userId);
            if (index > -1) {
                global.online.splice(index, 1);
            }
        }
    };

    /**
     * Busy update
     */
    that.setBusy = function(bus){
        global.incall = bus;
        var attr = lobbyStream.getAttributes();
        attr.busy = bus;
        lobbyStream.setAttributes(attr);
    };

    /**
     * Custom messages
     */
    that.sendMessage = function(userId, msg){
        msg.from = global.user._id;
        console.log("Sending message:");
        console.log("  user: "+userId);
        console.log(msg);
        lobbyStream.sendData({target: userId, data: msg});
    };

    that.receivedMessage = function(streamEvent){
        console.log("Message received");
        console.log(msg);
        var target = streamEvent.msg.target;
        var msg = streamEvent.msg.data;
        // Only my messages
        if (target === global.user._id) {
            switch (msg.type) {
                // Request for reload N notifications
                case 'reloadNotifications':
                    alert('aqui');
                    notificationController.askNotificationsCount();
                    break;
                // Request for reload user friends / groups
                case 'refreshUsers':
                    mainController.refreshContacts();
                    break;
                // Request for reload user chat
                case 'refreshChat':
                    chatsController.refreshChatMessages(0, msg.from);
                    break;
                // A call B
                case 'requestCall':
                    callingController.render(msg.from, msg.user, msg.image, msg.video);
                    break;
                // A cancels the call to B
                case 'requestCancelled':
                    callingController.close();
                    callingController.setAccepted(false);
                    break;
                // B answers A
                case 'responseCall':
                    if (msg.answer) {
                        that.initCall(msg.from, msg.video);
                        callingController.setAccepted(true);
                    } else {
                        callingController.setAccepted(false);
                    }
                    callingController.close();
                    break;
                // A send the created room to B
                case 'roomCall':
                    sidebarController.openChat(msg.from, true);
                    callController = new CallController(that, msg.from);
                    callController.createAndEnterRoom(msg.room, msg.video);
                    break;
            }
        }
    };

    that.initCall = function(from, video){
        sidebarController.openChat(from, true);
        callController = new CallController(that, from);
        var roomName = callController.generateRoomName();
        callController.createAndEnterRoom(roomName, video, function(){
            that.sendMessage(from, {type: "roomCall", from: global.user._id, room: roomName, video: video});
        });
    };

    that.init();
    return that;
}