function HomeController() {
    // bind event listeners to button clicks //
    var that = this;
    var controllers = [];

    that.addController = function(controller) {
        controllers.push(controller);
    };

    /**
     * Refresh Functions
     */
    that.refreshUserData = function() {
        for (var i=0; i<controllers.length; i++){
            if (typeof controllers[i].refreshUserData == 'function'){
                controllers[i].refreshUserData();
            }
        }
    };

    that.refreshAll = function(callbackUsers, callbackGroups) {
        that.refreshContacts(callbackUsers);
        that.refreshGroups(callbackGroups);
    };

    that.refreshContacts = function(callback) {
        $.ajax({
            url: "/users/friends",
            type: "GET",
            dataType: "json",
            cache: false,
            success: function (users) {
                if (users) {
                    // Save friends ids
                    for (var j in users){
                        var user = users[j];
                        global.friends.push(user._id);
                    }
                    // Notify controllers
                    for (var i=0; i<controllers.length; i++){
                        if (typeof controllers[i].refreshContacts == 'function'){
                            controllers[i].refreshContacts(users);
                        }
                    }
                    if (callback) callback();
                }
            },
            error: function (e) {
                alert('Error getting contacts');
            }
        });
    };

    that.refreshGroups = function(callback) {
        $.ajax({
            url: "/group/mine",
            type: "GET",
            dataType: "json",
            cache: false,
            success: function (groups) {
                if (groups) {
                    for (var i=0; i<controllers.length; i++){
                        if (typeof controllers[i].refreshGroups == 'function'){
                            controllers[i].refreshGroups(groups);
                        }
                    }
                    if (callback) callback();
                }
            },
            error: function () {
                alert('Error getting groups');
            }
        });
    };

    that.refreshChat = function(type, data) {
        if (type == "group"){
            for (var i=0; i<controllers.length; i++){
                if (typeof controllers[i].refreshChat == 'function'){
                    controllers[i].refreshChat(data);
                }
            }
        }
    };

    /**
     * Global function
     */
    that.userStatusUpdated = function(type, userId) {
        for (var i=0; i<controllers.length; i++){
            if (typeof controllers[i].userStatusUpdated == 'function'){
                controllers[i].userStatusUpdated(type, userId);
            }
        }
    };

    /**
     * Global function
     */
    that.refreshNotifications = function(count){
        for (var i=0; i<controllers.length; i++){
            if (typeof controllers[i].refreshNotifications == 'function'){
                controllers[i].refreshNotifications(count);
            }
        }
    };

    /**
     * Global function
     */
    that.windowResize = function(){
        for (var i=0; i<controllers.length; i++){
            if (typeof controllers[i].windowResize == 'function'){
                controllers[i].windowResize();
            }
        }
    };

    /**
     * Global function
     */
    that.setBusy = function(bus) {
        for (var i=0; i<controllers.length; i++){
            if (typeof controllers[i].setBusy == 'function'){
                controllers[i].setBusy(bus);
            }
        }
    };

    return that;
}
