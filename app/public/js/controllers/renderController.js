/**
 * Created by Adri on 10/01/2017.
 */
function RenderController() {
    var that = this;

    that.renderModalsLis = function(list, divId, customButton, confirmationCallback){
        if (!list){
            $(divId).append('<li class="list-group-item container-fluid"><p class="left bot0">Error</p></li>');
            return;
        }
        $(divId).html('');
        for (var i = 0; i < list.length; i++) {
            var item = list[i];
            var isGroup = true;
            if (list[i].friend){
                item = list[i].friend;
                isGroup = false;
            }

            // Front card
            var front = $('<div/>', {
                class: 'front'
            });
            var liTag = $('<li/>', {
                id: 'delete' + item._id,
                class: 'list-group-item container-fluid'
            });
            var imgTag = $("<div/>", {
                class: "left avatar-thumbnail-square",
                style: "width: 40px; height: 40px; background: url(/uploads/" + ((item.image) ? item.image : (isGroup ? "default-group.jpg" : "default.jpg")) + ") no-repeat center; background-size: cover;"
            });
            var pTag = $('<p/>', {
                class: "top10 left",
                text: (item.name ? item.name : item.email)
            });
            var button = "";
            if (customButton) {
                button = customButton(item._id);
            }
            $(front).append(imgTag, pTag, button);

            // Back card
            var back = $('<div/>', {
                class: 'back'
            });
            var pConf = $('<p/>', {
                class: "top10 left",
                style: "position: absolute;",
                text: "Are you sure?"
            });
            var butNo = $('<button>', {
                href: "#",
                class: "btn right btn-primary btn-xs top10",
                text: "No",
                'data-id': item._id
            }).on('click', function () {
                var deleteId = $(this).attr('data-id');
                $('#delete' + deleteId).flip('toggle');
            });
            var butYes = $('<button>', {
                href: "#",
                class: "btn right btn-danger btn-xs top10 right10",
                text: "Yes",
                'data-id': item._id
            });
            if (confirmationCallback){
                $(butYes).on('click', confirmationCallback)
            }
            $(back).append(pConf, butNo, butYes);

            $(liTag).append(front, back);
            $(divId).append(liTag);

            // Init Flip
            if (confirmationCallback) {
                $(divId + ' .list-group-item').flip({
                    trigger: 'manual',
                    autoSize: false
                });
            }
        }

        if (list.length == 0){
            $(divId).append('<li class="list-group-item container-fluid"><p class="left bot0">Empty</p></li>');
        }
    };

    return that;
}