function NotificationController() {
    // bind event listeners to button clicks //
    var that = this;

    that.acceptFriend = function(id){
        var action = $("#req"+id).attr('data-action');
        if (action === "waiting"){
            $("#req"+id).attr('data-action', "sending");
            $("#req"+id+" btn-success").blur();
            $("#req"+id+" btn-success").html("Sending");
            $.ajax({
                url: "/users/friend",
                type: "POST",
                data: {id: id},
                success: function (data) {
                    $("#req"+id).remove();
                    mainController.refreshContacts();
                    that.checkEmptyRequests();
                    that.askNotificationsCount();
                    licodeController.sendMessage(id, {type: "refreshUsers"});
                },
                error: function (err) {
                    $("#req"+id).attr('data-action', "waiting");
                    console.log(err);
                }
            });
        }
    };

    that.rejectFriend = function(id) {
        var action = $("#req"+id).attr('data-action');
        if (action === "waiting"){
            $("#req"+id).attr('data-action', "sending");
            $("#req"+id+" btn-danger").blur();
            $("#req"+id+" btn-danger").html("Sending");
            $.ajax({
                url: "/users/delete",
                type: "POST",
                data: {id: id},
                success: function (data) {
                    $("#req"+id).remove();
                    that.checkEmptyRequests();
                    that.askNotificationsCount();
                },
                error: function (err) {
                    $("#req"+id).attr('data-action', "waiting");
                    console.log(err);
                }
            });
        }
    };

    that.checkEmptyRequests = function() {
        if ($("#requests-list").html() == "") {
            $("#requests-list").append('<li class="list-group-item container-fluid"><p class="left">You don\'t have any friends request</p></li>');
        }
    };

    that.load = function(callback) {
        $.ajax({
            url: "/users/pending",
            type: "GET",
            dataType: "json",
            data: {logout: true},
            success: function (users) {
                if (users) {
                    $("#requests-list").html('');
                    for (var i = 0; i < users.length; i++) {
                        var friend = users[i];
                        console.log(friend);
                        var addBut = $("<a/>", {
                            class : "btn right btn-success btn-xs right10",
                            href: "#"
                        }).click(function(){
                            that.acceptFriend(friend._id);
                        }).html("Accept");

                        var rejectBut = $("<a/>", {
                            class : "btn right btn-danger btn-xs",
                            href: "#"
                        }).click(function(){
                            that.rejectFriend(friend._id);
                        }).html("Reject");

                        var pTag = $("<p/>", {
                            class : "left",
                            style: 'margin-bottom: auto;'
                        }).html((users[i].friend.name) ? users[i].friend.name: users[i].friend.email);

                        var liTag = $("<li/>", {
                            id: 'req'+users[i]._id,
                            class : "list-group-item container-fluid",
                            'data-action': "waiting"
                        }).append(pTag, rejectBut, addBut);
                        $("#requests-list").append(liTag);

                        //var addBut = '<a href="#" onclick="acceptFriend(\'' + users[i]._id + '\')" class="btn right btn-success btn-xs right10">Accept</a>';
                        //var rejectBut = '<a href="#" onclick="rejectFriend(\'' + users[i]._id + '\')" class="btn right btn-danger btn-xs">Reject</a>';
                        //$("#requests-list").append('<li id="req'+users[i]._id+'" data-action="waiting" class="list-group-item container-fluid"><p class="left">' + users[i].friend.email + '</p>' + rejectBut + addBut + '</li>');
                    }
                    that.checkEmptyRequests();
                    if (callback) callback(true);
                }
            },
            error: function () {
                if (callback) callback(false);
            }
        });
    };

    that.askNotificationsCount = function(){
        $.ajax({
            url: "/users/count/pending",
            type: "GET",
            dataType: "json",
            cache: false,
            success: function (result) {
                if (result) {
                    mainController.refreshNotifications(result.count);
                }
            },
            error: function () {
                alert('Error getting notifications count');
            }
        });
    };

    return that;
}
