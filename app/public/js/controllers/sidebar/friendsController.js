function FriendsController() {
    // bind event listeners to button clicks //
    var that = this;

    // Friends input search
    $('#friends-search').btsListFilter('#friends-input', {
        itemChild: 'p',
        emptyNode: function (a) {
            return '<li class="list-group-item container-fluid"><p class="left">No Results</p></li>';
        },
        resetOnBlur: !1
    });

    that.showError = function (text) {
        var errAlert = $('<div class="alert alert-dismissible alert-danger col-md-12 fade in">' +
            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
            '<strong>Input error!</strong> ' + text +
            '</div>');
        $(errAlert).appendTo($("#errorsAddFriends"));
        setTimeout(function () {
            $(errAlert).alert('close');
        }, 2500);
    };

    that.showSuccess = function (text) {
        var errAlert = $('<div class="alert alert-dismissible alert-success col-md-12 fade in">' +
            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
            '<strong>Success!</strong> ' + text +
            '</div>');
        $(errAlert).appendTo($("#errorsAddFriends"));
        setTimeout(function () {
            $('#friendsModal').modal('hide');
        }, 2500);
    };

    that.addFriend = function addFriend(id) {
        var action = $("#new" + id).attr('data-action');
        if (action === "Add") {
            $("#new" + id).attr('data-action', "sending");
            $("#new" + id + " button").blur();
            $("#new" + id + " button").html("Sending");
            $.ajax({
                url: "/users/friend",
                type: "POST",
                data: {id: id},
                success: function (data) {
                    $("#new" + id).attr('data-action', "pending");
                    $("#new" + id + " button").removeClass("btn-primary").addClass("btn-warning").html("Pending");
                    licodeController.sendMessage(id, {type: "reloadNotifications"});
                },
                error: function (err) {
                    $("#new" + id).attr('data-action', "add");
                    console.log(err);
                }
            });
        }
    }

    /*
     *   Show
     */
    $('#friendsModal').on('show.bs.modal', function (event) {
        $('#addFriendButton').addClass("active");
        $.ajax({
            url: "/users/nofriends",
            type: "GET",
            dataType: "json",
            data: {logout: true},
            success: function (users) {
                if (users) {
                    $("#friends-search").html('');
                    for (var i = 0; i < users.length; i++) {
                        //var button = '<a href="#" class="btn right btn-primary btn-xs">Add</a>';
                        var clas = 'btn right btn-primary btn-xs top10';
                        var text = "Add";
                        if (users[i].requested) {
                            clas = 'btn right btn-warning btn-xs no-clickable top10';
                            text = 'Pending';
                        }
                        var liTag = $('<li/>', {
                            id: 'new' + users[i]._id,
                            'data-action': text,
                            class: 'list-group-item container-fluid'
                        });
                        var imgTag = $("<div/>", {
                            class: "left avatar-thumbnail-square",
                            style: "width: 40px; height: 40px; background: url(/uploads/" + ((users[i].image) ? users[i].image : "default.jpg") + ") no-repeat center; background-size: cover;"
                        });
                        var button = $('<button>', {
                            href: "#",
                            class: clas,
                            text: text,
                            'data-id': users[i]._id
                        });
                        var pTag = $('<p/>', {
                            class: "top10 left",
                            text: (users[i].name ? users[i].name : users[i].email)
                        });
                        $(liTag).append(imgTag, pTag, button);
                        $(button).on('click', function (event) {
                            var id = $(this).attr('data-id');
                            that.addFriend(id);
                        });
                        $("#friends-search").append(liTag);
                    }
                }
            },
            error: function () {
                alert('error');
            }
        });
    });

    /*
     *   Hide
     */
    $('#friendsModal').on('hide.bs.modal', function (event) {
        $('#addFriendButton').removeClass("active");
        mainController.refreshContacts();
    });

    return that;
}
