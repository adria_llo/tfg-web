function GroupController() {
    // bind event listeners to button clicks //
    var that = this;

    // Group-friends input search
    $('#group-users-left').btsListFilter('#group-users-input', {
        itemChild: 'p',
        emptyNode: function (a) {
            return '<li class="list-group-item container-fluid"><p class="left">No Results</p></li>';
        },
        resetOnBlur: !1
    });

    that.generateLi = function (user, button) {
        var liTag = $('<li/>', {
            id: 'group' + user._id,
            class: 'list-group-item container-fluid'
        });
        var imgTag = $("<div/>", {
            class: "left avatar-thumbnail-square",
            style: "width: 40px; height: 40px; background: url(/uploads/" + ((user.image) ? user.image : "default.jpg") + ") no-repeat center; background-size: cover;"
        });
        var pTag = $('<p/>', {class: "top10 left", text: user.email});
        $(liTag).append(imgTag, pTag, button);
        return liTag;
    };

    that.generateBut = function (text) {
        return $('<a>', {
            href: "javascript:void(0)",
            class: "btn right btn-" + ((text === "X") ? "danger" : "primary") + " btn-xs top10",
            text: text
        });
    };

    that.showError = function (text) {
        var errAlert = $('<div class="alert alert-dismissible alert-danger col-md-12 fade in">' +
            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
            '<strong>Input error!</strong> ' + text +
            '</div>');
        $(errAlert).appendTo($("#errorsGroupDiv"));
        setTimeout(function () {
            $(errAlert).alert('close');
        }, 2500);
    };

    that.showSuccess = function (text) {
        var errAlert = $('<div class="alert alert-dismissible alert-success col-md-12 fade in">' +
            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
            '<strong>Success!</strong> ' + text +
            '</div>');
        $(errAlert).appendTo($("#errorsGroupDiv"));
        setTimeout(function () {
            $('#groupModal').modal('hide');
        }, 2500);
    };

    that.validateForm = function () {
        var name = $("#group-name-input").val();
        if (!name) {
            that.showError("Missing group name.");
            return false;
        }
        if ($("#group-users-right").children().size() == 0) {
            that.showError("You have to choose at least one member.");
            return false;
        }
        return true;
    };

    /*
     * Create group
     */
    $('#createGroupForm').ajaxForm({
        beforeSubmit: function (formData, jqForm, options) {
            if (that.validateForm() == false) {
                return false;
            }
            var ids = [];
            $("#group-users-right").children().each(function (idx, val) {
                var id = $(val).attr('id').replace("group", "");
                ids.push(id);
            });
            formData.push({name: 'ids', value: ids});
            return true;
        },
        success: function (data) {
            mainController.refreshGroups();
            mainController.refreshChat("group", data);
            $('#groupModal').modal('hide');
        },
        error: function (e) {
            that.showError("Error saving group.");
        }
    });

    /*
     *   Show
     */
    $('#groupModal').on('show.bs.modal', function (event) {
        var modal = $(this);
        var button = $(event.relatedTarget);
        var groupId = button.data('group');
        var mode = "edit";
        if (!groupId) {
            mode = "new";
            $('#createGroupButton').addClass("active");
            modal.find("input[name=groupId]").val('');
        } else {
            modal.find("input[name=groupId]").val(groupId);
        }
        modal.find("#group-users-right").html('');
        modal.find("#group-name-input").val('');

        var globalResponse = {};
        var doneFunction = function () {
            if (globalResponse.error) {
                alert(globalResponse.error);
                console.error(globalResponse.error);
                return;
            }
            if (globalResponse.friendships) {
                $("#group-users-left").html('');
                for (var i = 0; i < globalResponse.friendships.length; i++) {
                    var user = globalResponse.friendships[i].friend;

                    // Left li
                    var buttonL = that.generateBut(">>");
                    var liTagL = that.generateLi(user, buttonL);

                    // On click >>
                    $(buttonL).on('click', function (e) {
                        // Remove from left
                        $(liTagL).hide();

                        // Right �i
                        var buttonR = that.generateBut("X");
                        var liTagR = that.generateLi(user, buttonR);

                        // On click X
                        $(buttonR).on('click', function (e) {
                            // Remove from right
                            $(liTagR).remove();
                            // Append to left
                            $(liTagL).show();
                        });

                        // Append to right
                        $("#group-users-right").append(liTagR);
                    });

                    // Append to left
                    $("#group-users-left").append(liTagL);

                    if (globalResponse.group && globalResponse.members.indexOf(user._id) != -1) {
                        $(buttonL).click();
                    }
                }
            }
            if (globalResponse.group) {
                modal.find("#group-name-input").val(globalResponse.group.name);
                modal.find("#submitBut").html("Save");
            } else {
                modal.find("#submitBut").html("Create");
            }
        };

        if (mode == "new") {
            // Only get friends
            $.when(
                $.ajax({
                    url: "/users/friends",
                    type: "GET",
                    dataType: "json",
                    success: function (friendships) {
                        globalResponse.friendships = friendships;
                    },
                    error: function () {
                        globalResponse.error = "Error getting friends";
                    }
                })
            ).then(doneFunction);
        } else {
            // Get friends and group
            $.when(
                // Get the friends list
                $.ajax({
                    url: "/users/friends",
                    type: "GET",
                    dataType: "json",
                    success: function (friendships) {
                        globalResponse.friendships = friendships;
                    },
                    error: function () {
                        globalResponse.error = "Error getting friends";
                    }
                }),
                // Get the group
                $.ajax({
                    url: "/group/" + groupId,
                    type: "GET",
                    dataType: "json",
                    success: function (group) {
                        globalResponse.group = group;
                        globalResponse.members = [];
                        for (var i = 0; i < group.users.length; i++) {
                            globalResponse.members.push(group.users[i]._id);
                        }
                    },
                    error: function () {
                        globalResponse.error = "Error getting group details";
                    }
                })
            ).then(doneFunction);
        }
    });

    /*
     *   Hide
     */
    $('#groupModal').on('hide.bs.modal', function (event) {
        //var modal = $(this)
        //var groupId = modal.find('input[name=groupId]').val();
        //if (!groupId) {
        //    // New one
        //    $('#createGroupButton').removeClass("active");
        //}
        $('#createGroupButton').removeClass("active");
    });

    return that;
}
