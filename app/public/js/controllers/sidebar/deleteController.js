function DeleteController() {
    // bind event listeners to button clicks //
    var that = this;
    var renderController = new RenderController();

    // Friends input search
    $('#delete-friend-search').btsListFilter('#delete-friend-input', {
        itemChild: '.front p',
        emptyNode: function (a) {
            return '<li class="list-group-item container-fluid"><p class="left">No Results</p></li>';
        },
        resetOnBlur: !1
    });

    // Group input search
    $('#delete-group-input').btsListFilter('#delete-group-input', {
        itemChild: '.front p',
        emptyNode: function (a) {
            return '<li class="list-group-item container-fluid"><p class="left">No Results</p></li>';
        },
        resetOnBlur: !1
    });

    that.showError = function (text) {
        var errAlert = $('<div class="alert alert-dismissible alert-danger col-md-12 fade in">' +
            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
            '<strong>Input error!</strong> ' + text +
            '</div>');
        $(errAlert).appendTo($("#errorsDeletes"));
        setTimeout(function () {
            $(errAlert).alert('close');
        }, 2500);
    };

    that.showSuccess = function (text) {
        var errAlert = $('<div class="alert alert-dismissible alert-success col-md-12 fade in">' +
            '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
            '<strong>Success!</strong> ' + text +
            '</div>');
        $(errAlert).appendTo($("#errorsDeletes"));
        setTimeout(function () {
            $(errAlert).alert('close');
        }, 2500);
    };

    /*
     *   Show
     */
    $('#deleteModal').on('show.bs.modal', function (event) {
        $('#deleteUserGroupButton').addClass("active");
        $.ajax({
            url: "/users/friends",
            type: "GET",
            dataType: "json",
            data: {logout: true},
            success: function (friends) {
                renderController.renderModalsLis(friends, "#delete-friend-search", function(userId){
                    // Custom main button
                    var button = $('<button>', {
                        href: "#",
                        class: "btn right btn-danger btn-xs top10",
                        text: "Delete",
                        'data-id': userId
                    });
                    $(button).on('click', function () {
                        $('#delete' + userId).flip('toggle');
                    });
                    return button;
                }, function(){
                    // Delete User
                    var userId = $(this).attr('data-id');
                    $.ajax({
                        url: "/users/delete",
                        type: "POST",
                        data: {id: userId},
                        success: function () {
                            that.showSuccess("Friend deleted successfully!");
                            $('#delete' + userId).remove();
                            mainController.refreshContacts();
                            licodeController.sendMessage(userId, {type: "refreshUsers"});
                        },
                        error: function () {
                            that.showError("Error deleting friend");
                            $('#delete' + userId).flip('toggle');
                        }
                    });
                });
            },
            error: function () {
                renderController.renderModalsLis(null, "#delete-friend-search");
            }
        });

        $.ajax({
            url: "/group/mine",
            type: "GET",
            dataType: "json",
            cache: false,
            success: function (groups) {
                renderController.renderModalsLis(groups, "#delete-group-search", function(groupId){
                    var button = $('<button>', {
                        href: "#",
                        class: "btn right btn-danger btn-xs top10",
                        text: "Delete"
                    });
                    $(button).on('click', function () {
                        $('#delete' + groupId).flip('toggle');
                    });
                    return button;
                }, function(){
                    // Delete Group
                    var groupId = $(this).attr('data-id');
                    $.ajax({
                        url: "/group/delete",
                        type: "POST",
                        data: {id: groupId},
                        success: function () {
                            that.showSuccess("Group deleted successfully!");
                            $('#delete' + groupId).remove();
                            mainController.refreshGroups();
                        },
                        error: function () {
                            that.showError("Error deleting friend");
                            $('#delete' + groupId).flip('toggle');
                        }
                    });
                });
            },
            error: function () {
                renderController.renderModalsLis(null, "#delete-group-search");
            }
        });
    });

    /*
     *   Hide
     */
    $('#deleteModal').on('hide.bs.modal', function (event) {
        $('#deleteUserGroupButton').removeClass("active");
    });

    return that;
}
