function SidebarController() {
    // bind event listeners to button clicks //
    var that = this;

    /**
     * Init
     */
    that.init = function init() {
        $('#users-ul').on('hide.bs.collapse', function (e) {
            if ($('#users-header').hasClass('disabled')) {
                preventDefault(e);
            }
        });

        $('#users-ul-off').on('hide.bs.collapse', function (e) {
            if ($('#users-header-off').hasClass('disabled')) {
                preventDefault(e);
            }
        });

        $("#filterUsers").on('keyup', function(){
            var matcher = new RegExp($(this).val(), 'gi');
            $("#users-ul .user-tab").show().not(function(){
                return (matcher.test($(this).find('.filter-field').text())
                        && !$(this).hasClass("offline"));
            }).hide();
        });

        $("#filterUsers").on('keyup', function(){
            var matcher = new RegExp($(this).val(), 'gi');
            $("#users-ul-off .user-tab").show().not(function(){
                return (matcher.test($(this).find('.filter-field').text())
                        && $(this).hasClass("offline"));
            }).hide();
        });
    };

    /**
     * Opens a chat room
     */
    that.openChat = function openChat(chatId, inCall) {
        $(".user-tab").removeClass('hover');
        $(".user-tab[data-id=" + chatId + "]").addClass('hover');
        if ($('#chat'+chatId).length){
            // Chat exists
            chatsController.chatOpenned(chatId);
        } else {
            // Request render chat
            $.ajax({
                url: "/chat/" + chatId,
                type: "GET",
                success: function (data) {
                    $("#page-content-wrapper").append(data.html);
                    chatsController.chatOpenned(chatId);
                    if (inCall) {
                        chatsController.userStatusUpdated("inCall", chatId);
                    }
                },
                error: function (jqXHR) {
                    console.log(jqXHR.responseText + ' :: ' + jqXHR.statusText);
                }
            });
        }
    };

    /**
     * Hide all chat room
     */
    that.noChatsOppened = function noChatsOppened() {
        $(".user-tab").removeClass('hover');
    };

    /**
     * User status update
     */
    that.userStatusUpdated = function(type, userId){
        $(".user-tab[data-id="+userId+"] .avatar-thumbnail").removeClass("offline").removeClass("busy").removeClass("online").addClass(type);
        $(".user-tab[data-id="+userId+"]").removeClass("offline").removeClass("busy").removeClass("online").addClass(type);
    };

    that.refreshContacts = function refreshContacts(friendships) {
        // Save current user status
        var statusUsers = [];
        $("#users-ul").find(".user-tab").each(function (){
            var id = $(this).data('id');
            var status = 'online';
            if ($(this).hasClass('busy')) status = 'busy';
            if ($(this).hasClass('offline')) status = 'offline';
            statusUsers[id] = status;
        });

        // Users
        $("#users-ul").html('');
        $("#users-ul-off").html('');
        for (var i = 0; i < friendships.length; i++) {
            var user = friendships[i].friend;
            var extraClass = (typeof statusUsers[user._id] === 'undefined') ? "offline" : statusUsers[user._id];
            // User name
            var displayName = (user.name) ? user.name : user.email;
            var span = $("<span/>", {
                class: "filter-field"
            }).html(displayName);
            var divMd10 = $("<div/>", {
                class: "col-md-10"
            }).html(span);
            // User image
            var displayImage = (user.image) ? user.image : "default.jpg";
            var img = $("<div/>", {
                class: "avatar-thumbnail col-md-2 " + extraClass,
                style: "width: 40px; height: 40px; background: url(/uploads/" + displayImage + ") no-repeat center; background-size: cover; margin-left: -15px;"
            });
            var divMd2 = $("<div/>", {
                class: "col-md-2"
            }).html(img);
            // Fats call options
            divMd10.append(that.createHoverFastCall(user._id), that.createHoverFastVideo(user._id));
            // Final li
            extraClass += (window.location.href.indexOf(user._id) > -1) ? " hover" : "";
            var li = $('<li/>', {
                class: "user-tab row " + extraClass,
                'data-id': user._id
            }).append(divMd2, divMd10);
            $(li).on('click', function (event) {
                if (event.currentTarget) {
                    var userId = $(event.currentTarget).data("id");
                    that.openChat(userId, false);
                }
            });
            // Hover show / hide
            $(li).on('mouseover', function (event) {
                var userId = $(this).data("id");
                if ($(".user-tab[data-id="+userId+"] .avatar-thumbnail").hasClass("online") && !global.incall) {
                    $(this).find('.btn-call').css('display', '');
                }
            });
            $(li).on('mouseout', function (event) {
                $(this).find('.btn-call').css('display', 'none');
            });
            $("#users-ul").append(li);
        }
        $("#users-ul-off").html($("#users-ul").html());
        // if (friendships.length == 0) {
        //     $("#users-header").addClass("disabled");
        // } else {
        //     $("#users-header").removeClass("disabled");
        // }
    };

    that.createHoverFastCall = function(chatId){
        var iEar = $('<i/>', {
           class: 'glyphicon glyphicon-earphone'
        });
        return $('<button/>', {
            name: "fast-call",
            type: "button",
            class: "btn btn-call btn-circle btn-small",
            style: "right: 50px; position: absolute; display: none;",
            "data-id": chatId
        }).on('click', function () {
            var userId = $(this).data("id");
            that.startCall(userId, false);
        }).append(iEar);
    };

    that.createHoverFastVideo = function(chatId){
        var iVideo = $('<i/>', {
            class: 'glyphicon glyphicon-facetime-video'
        });
        return $('<button/>', {
            name: "fast-video",
            type: "button",
            class: "btn btn-call btn-circle btn-small",
            style: "right: 5px; position: absolute; display: none;",
            "data-id": chatId
        }).on('click', function () {
            var userId = $(this).data("id");
            that.startCall(userId, true);
        }).append(iVideo);
    };

    /**
     * Main Fast Call Buttons
     */
    that.startCall = function startCall(chatId, withVideo){
        mainController.setBusy(true);
        ion.sound.play("beep");
        var name = (global.user.name) ? global.user.name : global.user.email;
        var image = (global.user.image) ? global.user.image : "default.jpg";
        licodeController.sendMessage(chatId, {type: "requestCall", user: name, image: image, video: withVideo});
        licodeController.getCallingController().renderWait(chatId);
    };

    that.refreshGroups = function refreshGroups(groups) {
        if (true) return;
        // Groups
        $("#group-title").html("Groups");
        //$("#group-title").html("Groups (" + users.length + ")");
        $("#group-ul").html('');
        for (var i = 0; i < groups.length; i++) {
            var group = groups[i];
            var displayImage = (group.image) ? group.image : "default-group.jpg";
            var span = $("<span/>").html(group.name);
            var img = $("<div/>", {
                class: "avatar-thumbnail offline",
                style: "width: 40px; height: 40px; background: url(/uploads/" + displayImage + ") no-repeat center; background-size: cover;"
            });
            var li = $('<li/>', {
                class: "user-tab",
                'data-id': group._id
            }).append(img, span);
            $(li).on('click', function (event) {
                if (event.currentTarget) {
                    var groupId = $(event.currentTarget).data("id");
                    that.openChat(groupId, false);
                }
            });
            $("#group-ul").append(li);
        }
        if (groups.length == 0) {
            $("#group-header").addClass("disabled");
        } else {
            $("#group-header").removeClass("disabled");
        }
    };

    that.init();

    return that;
}
