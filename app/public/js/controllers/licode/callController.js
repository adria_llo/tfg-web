/**
 * Created by Adri on 24/12/2016.
 */
function CallController(lc, ui) {
    var that = this;

    var LicodeController = lc;
    var userId = ui;

    var room;
    var localStream;

    var rand = Math.floor(Math.random() * 26) + Date.now();

    that.generateRoomName = function(){
        return rand++;
    };

    that.createAndEnterRoom = function(roomName, hasVideo, callback){
        var config = {audio: true, video: hasVideo, data: true };
        localStream = Erizo.Stream(config);
        $.ajax({
            url: "/licode/createToken",
            type: "POST",
            data: {room: roomName},
            cache: false,
            success: function (response) {
                room = Erizo.Room({token: response});

                localStream.addEventListener('access-accepted', function () {
                    var subscribeToStreams = function (streams) {
                        var cb = function (evt){
                            console.log('Bandwidth Alert', evt.msg, evt.bandwidth);
                        };
                        for (var index in streams) {
                            var stream = streams[index];
                            // Subscribe only to my friends stream
                            if (localStream.getID() !== stream.getID()) {
                                room.subscribe(stream, {audio: true, video: hasVideo, slideShowMode: false});
                                stream.addEventListener('bandwidth-alert', cb);
                            }
                        }
                    };

                    room.addEventListener('room-connected', function (roomEvent) {
                        room.publish(localStream, {}, {id: global.user._id}, function(){
                            subscribeToStreams(roomEvent.streams);
                            if (callback) callback();
                        });
                    });

                    room.addEventListener("room-disconnected", function(roomEvent){
                    });

                    room.addEventListener('stream-subscribed', function(streamEvent) {
                        var stream = streamEvent.stream;
                        stream.play('remoteVideo'+userId);
                        chatsController.showLayout(userId, (stream.video) ? 3 : 5);
                    });

                    room.addEventListener('stream-added', function (streamEvent) {
                        var streams = [];
                        streams.push(streamEvent.stream);
                        subscribeToStreams(streams);
                    });

                    room.addEventListener('stream-removed', function (streamEvent) {
                        var stream = streamEvent.stream;
                        if (stream.elementID !== undefined) {
                            $("#"+stream.elementID).html("");
                            that.finishCall();
                        } else {
                            $("#localVideo"+userId).html("");
                        }
                    });

                    room.addEventListener('stream-failed', function (){
                        that.finishCall();
                    });

                    room.connect();

                    localStream.play('localVideo'+userId);
                });

                localStream.init();
            },
            error: function () {
                alert('Error entering room');
            }
        });
    };

    that.finishCall = function(){
        var roomId = room.roomID;
        localStream.close();
        room.unpublish(localStream);
        room.disconnect();
        that.deleteRoom(roomId);
        chatsController.showLayout(userId, 2);
    };

    that.deleteRoom = function(roomId){
        $.ajax({
            url: "/licode/deleteRoom",
            type: "POST",
            data: {room: roomId},
            cache: false,
            success: function () {
                mainController.setBusy(false);
            },
            error: function () {
                console.log("Error deleting room");
            }
        });
    };

    return that;
}
