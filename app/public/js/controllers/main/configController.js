function ConfigController() {
    // bind event listeners to button clicks //
    var that = this;

    var av = new AccountValidator();
    var avatar = "default.jpg";

    $('#account-form-update').ajaxForm({
        beforeSubmit : function(arr, $form, options){
            if (av.validateForm()){
                // Show loader
                $("#settings-pane .form-loader").find("button").hide();
                $("#settings-pane .form-loader").find(".gif-loader").show();
            } else {
                return false;
            }
        },
        success: function (data, status, xhr, $form) {
            if (status == 'success') {
                global.user = data;
            }
            $("#settings-pane .form-loader").find("button").show();
            $("#settings-pane .form-loader").find(".gif-loader").hide();
            $("#settings-pane").find("#pass-tf").val("");
            $("#settings-pane").find("#pass2-tf").val("");
            mainController.refreshUserData();
        },
        error: function (e) {
            alert('Error saving profile');
            console.log(e);
            mainController.refreshUserData();
        }
    });

    that.refreshUserData = function () {
        if (global.user.email) {
            $("#settings-pane #email-tf").attr("value", global.user.email);
        }
        if (global.user.name) {
            $("#settings-pane #name-tf").attr("value", global.user.name);
        }
        if (global.user.gender) {
            $("#settings-pane #" + global.user.gender + "Radio").addClass("active");
        }
        if (global.user.image && avatar !== global.user.image) {
            that.init();
        }
    };

    that.init = function() {
        if (global.user.image) {
            avatar = global.user.image;
        }
        $("#kv-avatar").html('<input id="avatar" name="avatar" type="file" class="file-loading">');
        $("#avatar").fileinput({
            overwriteInitial: true,
            maxFileSize: 10000,
            showClose: false,
            showCaption: false,
            browseLabel: '',
            removeLabel: '',
            browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
            removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
            removeTitle: 'Cancel or reset changes',
            elErrorContainer: '#kv-avatar-errors-1',
            msgErrorClass: 'alert alert-block alert-danger',
            defaultPreviewContent: '<img src="/uploads/' + avatar + '" alt="Your Avatar" style="width:160px">',
            layoutTemplates: {
                main2: '<div class="col-sm-8">{preview}</div> <div class="col-sm-2" style="height: 177px;"><div class="center-vertical">{remove} {browse}</div></div>',
                actions: '',
                footer: ''
            },
            allowedFileExtensions: ["jpg", "png", "gif"]
        });
    };

    that.init();

    that.refreshUserData();

    return that;
}