/**
 * Created by Adri on 24/12/2016.
 */
function WelcomeController() {
    var that = this;

    var serverUrl = '/';

    that.MainPane = $("#home-pane");
    that.localStream;
    that.room;

    that.init = function () {

        /**
         * Call yourself button
         */
        that.MainPane.find("#testConnectionButton").on('click', function(){
            $("#test-pane").show();
            $("#test-buttons").hide();
            $("#messages").html("");

            var config = {audio: true, video: true, data: true, videoSize: [640, 480, 640, 480]};

            that.localStream = Erizo.Stream(config);

            that.createToken('user', 'presenter', function (response) {
                var token = response;
                that.room = Erizo.Room({token: token});

                that.localStream.addEventListener('access-denied', function (event) {
                    that.printText('Mic and Cam KO');
                    that.printText('Cannot access to local media. Maybe you don\'t have any camera / microphone or you don\'t give them permissions.');
                });

                that.localStream.addEventListener('access-accepted', function () {
                    that.printText('Mic and Cam OK');
                    var subscribeToStreams = function (streams) {
                        for (var index in streams) {
                            var stream = streams[index];
                            that.room.subscribe(stream);
                        }
                    };

                    that.room.addEventListener('room-connected', function () {
                        that.printText('Connected to the room OK');
                        that.room.publish(that.localStream, {maxVideoBW: 300});
                    });

                    that.room.addEventListener('stream-subscribed', function (streamEvent) {
                        that.printText('Subscribed to your local stream OK');
                        var stream = streamEvent.stream;
                        stream.show('my_subscribed_video');

                    });

                    that.room.addEventListener('stream-added', function (streamEvent) {
                        that.printText('Local stream published OK');
                        var streams = [];
                        streams.push(streamEvent.stream);
                        subscribeToStreams(streams);
                    });

                    that.room.addEventListener('stream-removed', function (streamEvent) {
                        // Remove stream from DOM
                        that.printText('Removed stream KO');
                        var stream = streamEvent.stream;
                        if (stream.elementID !== undefined) {
                            var element = document.getElementById(stream.elementID);
                            document.body.removeChild(element);
                        }
                    });

                    that.room.addEventListener('stream-failed', function () {
                        console.log('STREAM FAILED, DISCONNECTION');
                        that.printText('STREAM FAILED, DISCONNECTION');
                        that.room.disconnect();
                    });

                    that.room.connect();

                    that.localStream.show('my_local_video');

                });

                that.localStream.init();
            });
        });

        /**
         * End yourself call button
         */
        that.MainPane.find("#stopTestConnectionButton").on('click', function(){
            $("#test-pane").hide();
            $("#test-buttons").show();

            var roomId = that.room.roomID;
            that.localStream.close();
            that.room.unpublish(that.localStream);
            that.room.disconnect();
            that.deleteRoom(roomId);
        });

    };

    that.createToken = function (userName, role, callback) {

        var req = new XMLHttpRequest();
        var url = serverUrl + 'licode/createToken/';
        var body = {username: userName, role: role, room: 'basicExampleRoom'};

        req.onreadystatechange = function () {
            if (req.readyState === 4) {
                callback(req.responseText);
            }
        };

        req.open('POST', url, true);
        req.setRequestHeader('Content-Type', 'application/json');
        req.send(JSON.stringify(body));
    };

    that.deleteRoom = function(roomId){
        $.ajax({
            url: "/licode/deleteRoom",
            type: "POST",
            data: {room: roomId},
            cache: false,
            success: function () {
            },
            error: function () {
                console.log("Error deleting room");
            }
        });
    };

    that.printText = function (text) {
        var value = $("#messages").html();
        text = text.replace("OK", "<strong>OK</strong>")
        text = text.replace("KO", "<strong>KO</strong>")
        $("#messages").html(value + '- ' + text + '<br>');
    };

    that.init();

    return that;
}
