/**
 * Created by Adri on 24/12/2016.
 */
function ChatsController() {
    var that = this;

    var chats = [];

    that.chatOpenned = function (chatIdNew) {
        $("#top-buttons-navbar").find("button").removeClass("my-focus");

        // Search for existing one
        var found = false;
        chats.forEach(function(pair){
            if (pair.id === chatIdNew){
                pair.controller.chatOpenned();
                found = true;
            }
        });
        // Not found, create new one
        if (!found){
            var obj = {};
            obj.id = chatIdNew;
            obj.controller = new ChatController(chatIdNew);
            chats.push(obj);
        }
    };

    /**
     * User status update
     */
    that.userStatusUpdated = function(type, userId){
        chats.forEach(function(pair){
            if (typeof pair.controller.userStatusUpdated == 'function' && pair.id === userId){
                pair.controller.userStatusUpdated(type);
            }
        });
    };

    /**
     * Refresh chat messages
     */
    that.refreshChatMessages = function(skip, userId) {
        chats.forEach(function(pair){
            if (typeof pair.controller.refreshChatMessages == 'function' && pair.id === userId){
                pair.controller.refreshChatMessages(skip);
            }
        });
    };

    /**
     * Show specific layout
     */
    that.showLayout = function(userId, layout){
        chats.forEach(function(pair){
            if (typeof pair.controller.showLayout == 'function' && pair.id === userId){
                pair.controller.showLayout(layout);
            }
        });
    };

    /**
     * Busy update
     */
    that.setBusy = function(bus){
        chats.forEach(function(pair){
            if (typeof pair.controller.setBusy == 'function'){
                pair.controller.setBusy(bus);
            }
        });
    };

    return that;
}
