/**
 * Created by Adri on 24/12/2016.
 */
function ChatController(ChatId) {
    var that = this;

    const Layout_VideoChat = 1;
    const Layout_Chat = 2;
    const Layout_Video = 3;
    const Layout_BigVideo = 4;
    const Layout_Phone = 5;

    that.ChatId = ChatId;
    that.MainPane = $("#chat"+that.ChatId);
    that.wasToggled = false;
    that.lastLayout = 3;

    that.init = function(){
        /**
         * Layout Buttons
         */
        that.MainPane.find("button[name^=layout]").on('click', function(){
            that.lastLayout = that.getCurrentLayout();
            that.showLayout($(this).attr('name').replace("layout", ""));
        });

        /**
         * Layout Button Close
         */
        that.MainPane.find("button[name=close]").on('click', function(){
            that.MainPane.find("button[name^=layout]").removeClass('active');
            that.MainPane.find("button[name=layout"+that.lastLayout+"]").addClass('active');
            $("#menu-toggle").show();
            if (that.wasToggled != $("#menu-toggle").hasClass('toggled')) $("#menu-toggle").click();
            that.showLayout(that.lastLayout);
        });

        /**
         * Silence button local
         */
        that.MainPane.find("#muteAudioLocal").on('click', function(){
            var localButton = $("#volume_local");
            localButton.click();
            $(this).find('img').attr('src',localButton.attr('src'));
        });

        /**
         * Silence button remote
         */
        that.MainPane.find("#muteAudioRemote").on('click', function(){
            var remoteButton = $("#remoteVideo"+that.ChatId + " img[id^=volume]");
            remoteButton.click();
            $(this).find('img').attr('src',remoteButton.attr('src'));
        });

        /**
         * Call Button
         */
        that.MainPane.find("button[name=call]").on('click', function(){
            mainController.setBusy(true);
            ion.sound.play("beep");
            var name = (global.user.name) ? global.user.name : global.user.email;
            var image = (global.user.image) ? global.user.image : "default.jpg";
            licodeController.sendMessage(that.ChatId, {type: "requestCall", user: name, image: image, video: false});
            licodeController.getCallingController().renderWait(that.ChatId);
        });

        /**
         * Video Button
         */
        that.MainPane.find("button[name=video]").on('click', function(){
            mainController.setBusy(true);
            ion.sound.play("beep");
            var name = (global.user.name) ? global.user.name : global.user.email;
            var image = (global.user.image) ? global.user.image : "default.jpg";
            licodeController.sendMessage(that.ChatId, {type: "requestCall",user: name, image: image, video: true});
            licodeController.getCallingController().renderWait(that.ChatId);
        });

        /**
         * End call Button
         */
        that.MainPane.find("button[name=end]").on('click', function(){
            licodeController.getCallController().finishCall();
            mainController.setBusy(false);
        });

        /**
         * Send message Button
         */
        that.MainPane.find("button[name=chat]").on('click', that.sendMessage);
        $("#inputChat"+that.ChatId).keyup(function(e){
            if(e.keyCode == 13) {
                that.sendMessage();
            }
        });

        that.showLayout(Layout_Chat);
        if (global.online.indexOf(that.ChatId) != -1){ // Contains
            that.userStatusUpdated("online");
        } else {
            that.userStatusUpdated('offline');
        }
    };

    that.chatOpenned = function () {
        // HTML actions
        if (!global.chatId && that.MainPane.css('display') == 'none') {
            history.pushState('', 'New Page Title', "/home/" + that.ChatId);
        }

        $('.main-pane').hide();
        that.MainPane.show();

        that.refreshChatMessages(0);
    };

    /**
     * Busy update
     */
    that.setBusy = function(bus){
        that.MainPane.find("button").hide();
        if (!bus && global.online.indexOf(that.ChatId) != -1) {
            that.MainPane.find("button[name=call]").show();
            that.MainPane.find("button[name=video]").show();
        }
    };

    that.showLayout = function(layout){
        console.log("Show layout: "+layout);
        that.MainPane.find("button[name^=layout]").removeClass('active');
        that.MainPane.find("button[name=layout"+layout+"]").addClass('active');
        that.MainPane.find("button[name=close]").hide();
        that.MainPane.find('.chat-header').show();
        that.MainPane.find('.conference-video').removeClass('fixSizeBig');
        that.MainPane.find(".phone-layout").hide();
        that.MainPane.find(".chat-layout").hide();
        switch (layout*1){
            case Layout_VideoChat:
                that.MainPane.find('.chat-conference').show();
                that.MainPane.find('.chat-messages').show();
                that.MainPane.find('.chat-footer').show();
                $("#menu-toggle").show();
                if (that.wasToggled != $("#menu-toggle").hasClass('toggled')) $("#menu-toggle").click();
                break;
            case Layout_Chat:
                that.MainPane.find('.chat-conference').hide();
                that.MainPane.find('.chat-messages').show();
                that.MainPane.find('.chat-footer').show();
                $("#menu-toggle").show();
                if (that.wasToggled != $("#menu-toggle").hasClass('toggled')) $("#menu-toggle").click();
                break;
            case Layout_Phone:
                that.MainPane.find('.chat-conference').hide();
                that.MainPane.find('.chat-messages').show();
                that.MainPane.find('.chat-footer').show();
                $("#menu-toggle").show();
                if (that.wasToggled != $("#menu-toggle").hasClass('toggled')) $("#menu-toggle").click();
                break;
            case Layout_Video:
                that.MainPane.find('.chat-conference').show();
                that.MainPane.find('.chat-messages').hide();
                that.MainPane.find('.chat-footer').hide();
                $("#menu-toggle").show();
                if (that.wasToggled != $("#menu-toggle").hasClass('toggled')) $("#menu-toggle").click();
                break;
            case Layout_BigVideo:
                that.MainPane.find('.chat-header').hide();
                that.MainPane.find('.chat-conference').show();
                that.MainPane.find('.chat-messages').hide();
                that.MainPane.find('.chat-footer').hide();
                that.MainPane.find('button[name=close]').show();
                that.MainPane.find('.conference-video').addClass('fixSizeBig');
                that.wasToggled = $("#menu-toggle").hasClass("toggled");
                $("#menu-toggle").hide();
                if (!that.wasToggled) $("#menu-toggle").click();
                break;
        }
        if (layout*1 == Layout_Phone){
            that.MainPane.find("button[name^=silence]").show();
            that.MainPane.find(".phone-layout").show();
            that.MainPane.find("button[name=layout2]").addClass('active');
            // Reset mute icons
            $("#muteAudioLocal").find('img').attr('src',$("#volume_local").attr('src'));
            $("#muteAudioRemote").find('img').attr('src',$("#remoteVideo"+that.ChatId + " img[id^=volume]").attr('src'));
        } else {
            that.MainPane.find(".chat-layout").show();
        }
        that.windowResize();
    };

    that.sendMessage = function() {
        var message = $("#inputChat"+that.ChatId).val();
        $.ajax({
            url: "/chat/individual/message",
            type: "POST",
            data: {
                to: that.ChatId,
                message: message
            },
            success: function (status) {
                $("#inputChat"+that.ChatId).val("");
                licodeController.sendMessage(that.ChatId, {type: "refreshChat"});
                that.refreshChatMessages(0);
            },
            error: function (err) {
                console.log(err);
                alert('Error sending message!');
            }
        });
    };

    that.clearChatMessages = function(){
        $("#messages"+that.ChatId).html("");
    };

    that.refreshChatMessages = function(skip){
        if (skip == 0){
            that.clearChatMessages();
        }
        $.ajax({
            url: "/chat/messages/" + that.ChatId + "/" + skip,
            type: "GET",
            success: function (result) {
                if (result) {
                    // Messages
                    result.messages.forEach(function(message){
                        var mDiv = $("<div/>", {
                            class: (global.user._id == message.from) ? "bubble bubble-alt" : "bubble",
                        }).html(message.message_body);
                        $("#messages"+that.ChatId).prepend(mDiv);
                    });
                    // Show more
                    if (result.messages.length >= result.limit) {
                        var showMore = $("<button/>", {
                            class: "btn btn-primary bubble-load",
                            type: "button",
                            'data-next': result.next
                        }).on('click', function(){
                            var data_next = $(this).attr('data-next');
                            that.refreshChatMessages(data_next);
                        }).html("Load more");
                        $("#messages"+that.ChatId).prepend(showMore);
                    }
                    // Scroll
                    var element = document.getElementById("scroll"+that.ChatId);
                    var scroll = element.scrollHeight;
                    if (skip != 0){
                        var iTop = $("#messages"+that.ChatId).offset().top;
                        var sTop = $("#messages"+that.ChatId+" button[data-next="+skip+"]").offset().top;
                        var hTop = $("#messages"+that.ChatId+" button[data-next="+skip+"]").outerHeight();
                        scroll = sTop - iTop - hTop;
                        $("#messages"+that.ChatId+" button[data-next="+skip+"]").remove();
                    }
                    element.scrollTop = scroll;
                }
            },
            error: function (err) {
                console.log(err);
                alert('Error getting messages!');
            }
        });
    };

    /**
     * User status update
     */
    that.userStatusUpdated = function(type){
        console.log("------ " + that.ChatId + " "+type);
        if (type === 'online'){
            that.MainPane.find("button").hide();
            that.MainPane.find("button[name=call]").show();
            that.MainPane.find("button[name=video]").show();
            that.MainPane.find(".calling-layout").hide();
        } else if (type === 'offline' || type === 'busy') {
            that.MainPane.find("button").hide();
            that.MainPane.find(".calling-layout").hide();
        } else if (type === "inCall") {
            that.MainPane.find("button").hide();
            that.MainPane.find("button[name=end]").show();
            that.MainPane.find("button[name^=layout]").show();
            that.MainPane.find(".calling-layout").show();
        }
        that.windowResize();
    };

    /**
     * Refresh group chat
     */
    that.refreshChat = function (group) {
        var pane = $("#chat" + group._id);
        $(pane).find(".group-title").html(group.name);
        $(pane).find(".group-members").html("");
        var fullHtml = "";
        for (var i = 0; i < group.users.length; i++) {
            fullHtml += '<li class="col-sm-4 member-tab" data-id="'+group.users[i]._id+'">' +
                '<div class="avatar-thumbnail-square offline left" style="width: 25px; height: 25px; background: url(/uploads/' + group.users[i].image + ') no-repeat center; background-size: cover;"></div>' +
                '<span>' + group.users[i].name + '</span>' +
                '</li>';
        }
        $(pane).find(".group-members").html(fullHtml);
    };

    /**
     * Refresh height
     */
    that.windowResize = function(){
        var hTop = that.MainPane.find(".chat-header").outerHeight();
        var hBottom = (that.MainPane.find(".chat-footer").css('display') !== "none") ? that.MainPane.find(".chat-footer").outerHeight() : 0;
        if (that.getCurrentLayout() == Layout_VideoChat){
            var height = that.MainPane.find(".chat-footer").offset().top - that.MainPane.find(".chat-header").offset().top - hTop;
            var newBottom = hBottom + height * 0.35;
            var newTop = hTop + height * 0.65;
            console.log("height:    "+height);
            console.log("newTop:    "+newTop);
            console.log("newBottom: "+newBottom);
            that.MainPane.find(".chat-conference").css("top", hTop).css("bottom", newBottom);
            that.MainPane.find(".chat-messages").css("top", newTop).css("bottom", hBottom);
        } else {
            $("#chat"+that.ChatId+" .chat-body").css("top", hTop).css("bottom", hBottom);
        }
        // Height
        var conferenceHeight = that.MainPane.find(".chat-conference").outerHeight();
        that.MainPane.find(".chat-conference").find('.conference-video').removeClass('fixSizeSmall');
        that.MainPane.find(".chat-conference").find('.conference-video').removeClass('fixSizeMedium');
        that.MainPane.find(".chat-conference").find('.conference-video').removeClass('fixSizeLarge');
        if (conferenceHeight < 390) {
            that.MainPane.find(".chat-conference").find('.conference-video').addClass('fixSizeSmall');
        } else if (conferenceHeight < 510) {
            that.MainPane.find(".chat-conference").find('.conference-video').addClass('fixSizeMedium');
        }
    };

    /**
     * Refresh height
     */
    that.getCurrentLayout = function(){
        if (that.MainPane.find("button[name^=layout].active").attr('name')) {
            return that.MainPane.find("button[name^=layout].active").attr('name').replace("layout","") * 1;
        }
        return Layout_VideoChat;
    };

    that.init();
    that.chatOpenned();

    return that;
}
