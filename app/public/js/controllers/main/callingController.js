/**
 * Created by Adri on 24/12/2016.
 */
function CallingController(lc) {
    var that = this;
    var userId;
    var licodeController = lc;
    var isWaiting;
    var accepted;
    var hasVideo;

    that.render = function (tmpId, username, image, hv) {
        isWaiting = false;
        hasVideo = hv;
        // Buttons
        if (hasVideo) {
            $("#callingModal button[name=accept]").hide();
            $("#callingModal button[name=video]").show();
        } else {
            $("#callingModal button[name=accept]").show();
            $("#callingModal button[name=video]").hide();
        }
        $("#callingModal button[name=reject]").show();
        $("#callingModal button[name=cancel]").hide();
        // Info
        userId = tmpId;
        $("#callerTitle").html((hasVideo) ? "Incoming video call" : "Incoming call");
        $("#callerName").html(username);
        $("#callerImage").css({
            background: 'url(/uploads/' + image + ') no-repeat center',
            'background-size': 'cover'
        });
        $("#callingModal").modal('show');
        // Hidden actions
        if (document.hidden) {
            // Tab title
            $.titleAlert("Incoming call!", {duration: 5000});
            // Notification
            if (Notification.permission === "granted") {
                var notification = new Notification((hasVideo) ? "Incoming video call from:" : "Incoming call from:", {
                    icon: '/uploads/' + image,
                    body: "  " + username,
                });
                notification.addEventListener("click", function () {
                    window.focus();
                    notification.close();
                });
                setTimeout(function () {
                    notification.close()
                }, 5000);
            }
        }
    };

    that.renderWait = function (tmpId) {
        isWaiting = true;
        userId = tmpId;
        $.ajax({
            url: "/users/info/" + tmpId,
            type: "GET",
            dataType: "json",
            cache: false,
            success: function (user) {
                if (user) {
                    // Info
                    $("#callerTitle").html("Calling");
                    $("#callerName").html(user.name);
                    $("#callerImage").css({
                        background: 'url(/uploads/' + user.image + ') no-repeat center',
                        'background-size': 'cover'
                    });
                    $("#callingModal").modal('show');
                }
            },
            error: function (e) {
                alert('Error getting user info');
            }
        });

        // Buttons
        $("#callingModal button[name=accept]").hide();
        $("#callingModal button[name=video]").hide();
        $("#callingModal button[name=reject]").hide();
        $("#callingModal button[name=cancel]").show();
    };

    that.close = function () {
        $("#callingModal").modal('hide');
    };

    that.setAccepted = function (acc) {
        accepted = acc;
    };


    /**
     *   Show
     */
    $('#callingModal').on('show.bs.modal', function (event) {
        if (isWaiting) {
            ion.sound.play("beep");
        } else {
            ion.sound.play("iphone");
        }
        mainController.setBusy(true);
    });

    /**
     *   Hide
     */
    $('#callingModal').on('hide.bs.modal', function (event) {
        ion.sound.stop("iphone");
        ion.sound.stop("beep");
        if (!accepted) {
            mainController.setBusy(false);
        } else {
            mainController.userStatusUpdated("inCall", userId);
        }
    });


    /**
     * Accept call buttons
     */
    that.acceptCall = function (){
        licodeController.sendMessage(userId, {type: "responseCall", answer: true, video: hasVideo});
        that.setAccepted(true);
        $("#callingModal").modal('hide');
    };
    $("#callingModal button[name=accept]").on('click', that.acceptCall);
    $("#callingModal button[name=video]").on('click', that.acceptCall);

    /**
     * Reject button
     */
    $("#callingModal button[name=reject]").on('click', function () {
        licodeController.sendMessage(userId, {type: "responseCall", answer: false});
        that.setAccepted(false);
        $("#callingModal").modal('hide');
    });

    /**
     * Cancel button
     */
    $("#callingModal button[name=cancel]").on('click', function () {
        licodeController.sendMessage(userId, {type: "requestCancelled"});
        that.setAccepted(false);
        $("#callingModal").modal('hide');
    });

    return that;
}
